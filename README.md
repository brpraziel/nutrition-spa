# RECIPE NUTRITION CALCULATOR SPA

## Description

Final Project Assignment for 'Telerik Academy Alpha' with JavaScript: Design and implement single-page web application that will allow restaurant chefs to create and manage recipes composed of products with known nutrition values.

### User Stories

- x Authenticate users - login
- x Users can CRUD recipes
- x Users can search list of recipes by name or filter by nutrition value
- x Users can search for product and filter by product group
- x Users can print recipes
- x Users can add recipes in a calendar
- x Users can analyze text and this will redirect them to a create recipe view (WorkInProgress)
- x Users can bookmark recipes (WorkInProgress)

### Stack

Front-end:
- SPA - Angular
Git repository: https://gitlab.com/brpraziel/nutrition-spa

Back-end:
- Database - MySQL
- REST API - NestJS
Git repository: https://gitlab.com/ciiara/nutrition-api

----------

# Getting started

## Installation

Run
    `git clone https://gitlab.com/brpraziel/nutrition-spa.git`

Get into the project's folder
    `cd nutrition-spa`

## In order to run the project

1. run `npm install` to install all dependencies
2. run `ng s -o` to open the project in a new tab in your browser
* Make sure that you have the back-end and database running too and you are logged in

## Scripts

- `ng s` or `ng s -o` - Starts the application in your browser
- `ng test` - Runs the unit tests

## Documentation

This readme file along with some screenshots serve the purpose of documenting this SPA
A folder named 'screenshots' can be found on top level in our project's files directory

----------

# Authors
Martin Nedyalkov
Silviya Velikova
