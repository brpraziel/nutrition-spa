import { Component, OnInit } from '@angular/core';
import { NavbarService } from '../../core/services/navbar.service';
import { Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import { RecipeService } from 'src/app/core/services/recipe.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  /* delete after test the filled property */
  filled;
  allRecipes;

  constructor(
    private readonly navSidebarService: NavbarService,
    private readonly recipeService: RecipeService,
    private readonly route: ActivatedRoute,
    public router: Router) {
  }

  search1 = (text$: Observable<string>) =>
  text$.pipe(
    debounceTime(200),
    distinctUntilChanged(),
    map(term => term.length < 2 ? []
      : this.allRecipes.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
  )

  fillArr(){}

  ngOnInit() {
    this.recipeService.getAllRecipes().subscribe(params => {
      this.allRecipes=params.map(x=>{return x.title})
      
    });
  }

  toRecipe(event){
    this.router.navigate([`/recipes/details/${event.item}`]);
    
  }

  toggleSidebar() {
    this.navSidebarService.triggerToggleEvent();
  }

}
