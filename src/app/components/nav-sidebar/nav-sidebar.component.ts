import { Component, OnInit, OnDestroy } from '@angular/core';
import { NavbarService } from 'src/app/core/services/navbar.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-nav-sidebar',
  templateUrl: './nav-sidebar.component.html',
  styleUrls: [ './nav-sidebar.component.css' ],
  })
export class NavSidebarComponent implements OnInit, OnDestroy {
    _opened: boolean = false;
    _modeNum: number = 0;
    _positionNum: number = 0;
    _dock: boolean = false;
    _closeOnClickOutside: boolean = false;
    _closeOnClickBackdrop: boolean = true;
    _showBackdrop: boolean = true;
    _animate: boolean = true;
    _trapFocus: boolean = true;
    _autoFocus: boolean = true;
    _keyClose: boolean = false;
    _autoCollapseHeight: number = null;
    _autoCollapseWidth: number = null;
    _MODES: Array<string> = ['over', 'push', 'slide'];
    _POSITIONS: Array<string> = ['left', 'right', 'top', 'bottom'];

    private subscription: Subscription;

    constructor(
       private readonly navSidebarService: NavbarService,
    ) {}

    ngOnInit() {
        this.subscription = this.navSidebarService.toggleEvent$.subscribe(
            state => {
                this._opened = state;
            },
            error => {
                console.log('Please investigate a problem in ngOnInit in the nav-sidebar ts file.');
            }
        );

    }

    ngOnDestroy() {
     this.subscription.unsubscribe();
    }

    _toggleMode(): void {
        this._modeNum++;
        if (this._modeNum === this._MODES.length) {
            this._modeNum = 0;
        }
    }

    _toggleAutoCollapseHeight(): void {
        this._autoCollapseHeight = this._autoCollapseHeight ? null : 500;
    }

    _toggleAutoCollapseWidth(): void {
        this._autoCollapseWidth = this._autoCollapseWidth ? null : 500;
    }

    _togglePosition(): void {
        this._positionNum++;
        if (this._positionNum === this._POSITIONS.length) {
            this._positionNum = 0;
        }
    }

    _toggleDock(): void {
        this._dock = !this._dock;
    }

    _toggleCloseOnClickOutside(): void {
        this._closeOnClickOutside = !this._closeOnClickOutside;
    }

    _toggleCloseOnClickBackdrop(): void {
        this._closeOnClickBackdrop = !this._closeOnClickBackdrop;
    }

    _toggleShowBackdrop(): void {
        this._showBackdrop = !this._showBackdrop;
    }

    _toggleAnimate(): void {
        this._animate = !this._animate;
    }

    _toggleTrapFocus(): void {
        this._trapFocus = !this._trapFocus;
    }

    _toggleAutoFocus(): void {
        this._autoFocus = !this._autoFocus;
    }

    _toggleKeyClose(): void {
        this._keyClose = !this._keyClose;
    }

    _onOpenStart(): void {
        console.info('Sidebar opening');
    }

    _onOpened(): void {
        console.info('Sidebar opened');
    }

    _onCloseStart(): void {
        console.info('Sidebar closing');
    }

    _onClosed(): void {
        console.info('Sidebar closed');
    }

    _onTransitionEnd(): void {
        console.info('Transition ended');
    }

    _onBackdropClicked(): void {
        console.info('Backdrop clicked');
    }
}
