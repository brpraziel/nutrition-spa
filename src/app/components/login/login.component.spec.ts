import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { TestBed } from '@angular/core/testing';
import { AuthService } from '../../core/services/auth.service';
import { LoginComponent } from './login.component';
import { Router, RouterModule} from '@angular/router';
import { SharedModule } from '../../../app/shared/shared.module';
import { FormsModule, NgModel } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Observable } from 'rxjs';
import { NotificatorService } from 'src/app/core/services/notificator.service';
//import { PostsRoutingModule } from 'src/app/posts/posts-routing.module';
describe('LoginComponent', () => {
    const component= LoginComponent
    let fixture;
    const authService = jasmine.createSpyObj('AuthService', ['login']);
    const router = jasmine.createSpyObj('Router', ['navigate']);
    const notificator= jasmine.createSpyObj('NotificatorService',['success','error'])
    
    beforeEach(() => TestBed.configureTestingModule({
        providers: [
            {
                provide: AuthService,
                useValue: authService,
            },
            // {
            //     provide: Router ,
            //     useValue: router,
            // },
            {
                provide: NotificatorService ,
                useValue: notificator,
            },
            
        ],imports: [
            SharedModule,
            FormsModule,
            RouterModule.forRoot([]),
          ],
          declarations: [LoginComponent]
    }));

    // afterEach(() => {
    //     if (fixture.nativeElement && 'remove' in fixture.nativeElement) {
    //       (fixture.nativeElement as HTMLElement).remove();
    //     }
    //   });

    it('should create', () => {
        expect(component).toBeTruthy();
      });


      it('login should call authLogin', async() => {
        const email="test@test.com"
        const password="testPassword"
        const user ={email,password}
        
        fixture = TestBed.createComponent(LoginComponent);
        const app = fixture.debugElement.componentInstance;
        authService.login.calls.reset()
        authService.login.and.returnValue(of(user));
        app.login(email,password)
        await fixture.detectChanges();
        expect(authService.login).toHaveBeenCalledTimes(1)
      });

      it('authLogin should be called with correct parameters ', async() => {
        const email="test@test.com"
        const password="testPassword1!"
        const user ={email,password}
        
        fixture = TestBed.createComponent(LoginComponent);
        const app = fixture.debugElement.componentInstance;
        authService.login.calls.reset()
        authService.login.and.returnValue(of(user));
        
        app.login(email,password)
        await fixture.detectChanges();
        expect(authService.login).toHaveBeenCalledWith(user)
      });

      it('login should call refer to /recipes on succses', async() => {
        const email="test@test.com"
        const password="testPassword"
        const user ={email,password}
        
        fixture = TestBed.createComponent(LoginComponent);
        const app = fixture.debugElement.componentInstance;
        authService.login.and.returnValue(of(user));
        const route = ['/recipes'];
        router.navigate.calls.reset();
        app.login(email,password)
        await fixture.detectChanges();
        expect(notificator.success).toHaveBeenCalled()
      });
      
      });

