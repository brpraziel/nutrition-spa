import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from '../../core/services/auth.service';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { UserLogin } from '../../common/models/user-login.dto';
import { NotificatorService } from '../../core/services/notificator.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {
  condition = false;

  public wrongCredentials: any;
  public loginSubscription: Subscription;

  constructor(
    private readonly authService: AuthService,
    private readonly notificator: NotificatorService,
    private readonly router: Router,
  ) { }

  ngOnInit() {
  }

  public login(email: string, password: string) {
    const user: UserLogin = { email, password };
    this.loginSubscription = this.authService.login(user).subscribe(
    (data) => {
      console.log(user);
      
      this.notificator.success(`Welcome, Chef`);
      this.router.navigate(['/recipes']);
    },
    (err: any) => {
      this.wrongCredentials="Wrong Username or Password";
      this.notificator.error('Please check again your credentials.');
    });
  }

  ngOnDestroy(): void {
    if (this.loginSubscription) {
      this.loginSubscription.unsubscribe();
    }
  }
}
