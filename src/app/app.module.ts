import { NgModule } from '@angular/core';
import { environment } from '../environments/environment';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AngularFireModule } from '@angular/fire';
import { AuthComponent } from './auth/auth.component';
import { LoginComponent } from './components/login/login.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbTypeaheadModule } from '@ng-bootstrap/ng-bootstrap';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenInterceptorService } from './auth/interceptors/token-interceptor.service';
import { JwtModule } from '@auth0/angular-jwt';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { CommonModule } from '@angular/common';
import { CoreModule } from './core/core.module';
import { SharedModule } from './shared/shared.module';
import { SidebarModule } from 'ng-sidebar';
import { NavSidebarComponent } from './components/nav-sidebar/nav-sidebar.component';
import { BrowserModule } from '@angular/platform-browser';
import { IngredientDataServices } from './core/services/ingredient.service';
import { CalculationService } from './core/services/calculation.service';
import { ItemService } from './core/services/item.service';
import { AngularFirestore } from 'angularfire2/firestore';
import { AuthGuardService } from './auth/guards/auth-guard.service';
import { IngredientComponent } from '../app/features/ingredient/ingredient.component';
import { SubrecipeComponent } from './features/subrecipe/subrecipe.component';
import { PrintRecipeResolverService } from './features/recipe/print-recipe-resolver.service';
import { ServiceWorkerModule } from '@angular/service-worker';

export function tokenGetter() {
  return localStorage.getItem('access_token');
}
@NgModule({
  declarations: [
    AppComponent,
    AuthComponent,
    LoginComponent,
    NavSidebarComponent,
    NavbarComponent,
    NotFoundComponent,
    SubrecipeComponent,
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    CoreModule,
    CommonModule,
    SharedModule,
    AngularFireModule.initializeApp(environment.firebase,"nutrition-6fc5e"),
    //AngularFireModule,
    AppRoutingModule,
    TypeaheadModule.forRoot(),
    NgbTypeaheadModule,
    SidebarModule.forRoot(),
    JwtModule.forRoot({
      config: {
        tokenGetter,
        whitelistedDomains: ['example.com'],
        blacklistedRoutes: ['/recipes']
      }
    }),
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
  ],
  providers: [
    ItemService,
    AngularFirestore,
    AuthGuardService,
    IngredientDataServices,
    CalculationService,
    PrintRecipeResolverService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true,
    }],
  entryComponents: [
    IngredientComponent,
    SubrecipeComponent,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
