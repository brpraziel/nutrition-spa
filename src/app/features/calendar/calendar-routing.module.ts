import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppCalendarComponent } from './calendar.component';
import { RecipeResolveService } from '../recipe/recipe-resolver.service';

const routes: Routes = [
  {
    path: '',
    component: AppCalendarComponent,
    resolve: {
      recipes_list: RecipeResolveService,
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CalendarRoutingModule {}
