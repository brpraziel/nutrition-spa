import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { AppCalendarComponent } from './calendar.component';
import { CalendarRoutingModule } from './calendar-routing.module';

@NgModule({
  declarations: [
    AppCalendarComponent,
  ],
  imports: [
    SharedModule,
    CalendarRoutingModule,
  ],
})
export class AppCalendarModule { }
