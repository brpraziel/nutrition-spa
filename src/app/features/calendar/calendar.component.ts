import { Component, ViewChild, OnInit } from '@angular/core';
import { FullCalendarComponent } from '@fullcalendar/angular';
import { EventInput } from '@fullcalendar/core';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGrigPlugin from '@fullcalendar/timegrid';
import interactionPlugin from '@fullcalendar/interaction'; // for dateClick
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';
import { ActivatedRoute } from '@angular/router';
import { Recipe } from 'src/app/common/interfaces/recipe';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.css']
})
export class AppCalendarComponent implements OnInit {
  allRecipes: Recipe[];
  recipeNames: {} = {};

  constructor(
    private readonly modalService: NgbModal,
    private readonly route: ActivatedRoute,
  ) {}

  @ViewChild('mycalendar', null) calendarComponent: FullCalendarComponent; // the #calendar in the template

  calendarVisible = true;
  calendarPlugins = [dayGridPlugin, timeGrigPlugin, interactionPlugin];
  calendarWeekends = true;
  calendarEvents: EventInput[] = [
    { title: 'Recipe for the day', start: new Date(), color: '#aed407' }
  ];

  // gotoPast() {
  //   let calendarApi = this.calendarComponent.getApi();
  //   calendarApi.gotoDate('2000-01-01'); // call a method on the Calendar object
  // }

  async handleDateClick(arg) {
    // opens dialogue window
    const {value: recipeName} = await Swal.fire({
      title: 'Available recipes:',
      input: 'select',
      inputOptions: this.recipeNames,
      inputPlaceholder: 'Select a recipe',
      showCancelButton: true,
      confirmButtonText: 'Save',
      cancelButtonText: 'Cancel',
      confirmButtonColor: '#6593f5',
      cancelButtonColor: '#aed407',
      reverseButtons: false
    });

    if (recipeName) {
      Swal.fire(
        `${recipeName}`,
        'created event',
        'success'
      );

      this.calendarEvents = this.calendarEvents.concat({ // add new event data. must create new array
        title: recipeName,
        start: arg.date,
        allDay: arg.allDay,
        color: '#aed407',
      });

    }

  }

  addEvent() {
    this.calendarEvents.push(
      { title: 'Recipe Test', date: '2019-07-02' },
    );
  }

  modifyTitle(eventIndex, newTitle) {
    this.calendarEvents[eventIndex].title = newTitle;
  }

  ngOnInit() {
    this.route.data.subscribe(
      data => {
        // load all recipes
        if (data.recipes_list) {
          this.allRecipes = data.recipes_list;
        } else {
          return console.log('Error in retrieval of all recipes.');
        }
      },
      error => {
        console.log('An error ocurred while taking data from recipe\'s resolvers.');
      }
    );

    this.allRecipes.map( recipe => {
      const recipeName = recipe.title;
      this.recipeNames[recipeName] = recipeName;
    });
  }

}
