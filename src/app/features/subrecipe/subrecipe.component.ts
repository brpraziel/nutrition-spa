import { Component, OnInit, Input } from '@angular/core';
import { RecipeService } from 'src/app/core/services/recipe.service';
import { ActivatedRoute } from '@angular/router';
import { Recipe } from 'src/app/common/interfaces/recipe';
import Swal from 'sweetalert2';


export interface myinterface2 {
  removeSubrecipeComponent(subrecipeIndex: number);
}


@Component({
  selector: 'app-subrecipe',
  templateUrl: './subrecipe.component.html',
  styleUrls: ['./subrecipe.component.css']
})
export class SubrecipeComponent implements OnInit {
  item;
  
  @Input()
  amount

  @Input()
  name
  
  @Input()
  incrementedValue

  public receipeToReturn
  public allRecipes
  public overallNutrition
  public compInteraction: myinterface2;
  public subrecipeIndex: number;
  public selfRef1: SubrecipeComponent ;
  public showCkalNumber:number
  public amount1:number
  constructor(private recipeService:RecipeService,
    private readonly route: ActivatedRoute,) { }

    public increments(value){
      this.incrementedValue=value
      
    }

  ngOnInit() {
    this.recipeService.getAllRecipes().subscribe(params=>{
      this.allRecipes=params})
    if(this.name===undefined){
      return
    }
    this.getRecipe(name)
  }
  

  removeMe(subrecipeIndex) {
    this.compInteraction.removeSubrecipeComponent(subrecipeIndex)
  }
  removeField(index){
    Swal.fire ({
      title: 'Are you sure?',
      text: "Do you want to remove this field?",
      type: 'warning', 
      showCancelButton: true, 
      confirmButtonColor: '#00B96F',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, remove!' 
    }).then((result) => {
      if (result.value) {
        this.removeMe(index)
        Swal.fire(
          'Deleted!',
          'Your Subrecipe has been deleted.',
          'success'
        )
      }
    })

  }
  getRecipe(value){
    this.receipeToReturn=value
    this.getSubrecipeDetailes(value)
    }

  getSubrecipeDetailes(recipe){
    this.recipeService.getSingleRecipeByTitle(recipe).subscribe(params=>{
this.overallNutrition=params[0].overallNutrition
    })
  }


showCkal(){
this.showCkalNumber=(+this.overallNutrition/100)* +this.incrementedValue
}
}