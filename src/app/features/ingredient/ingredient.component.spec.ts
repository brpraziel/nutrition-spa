import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IngredientComponent } from './ingredient.component';
import { IngredientDataServices } from 'src/app/core/services/ingredient.service';
import { CalculationService } from 'src/app/core/services/calculation.service';
import { FormsModule } from '@angular/forms';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { NgbTypeaheadModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { of } from 'rxjs';

describe('IngredientComponent', () => {
  let component: IngredientComponent;
  let fixture: ComponentFixture<IngredientComponent>;
const ingredientDataServices=jasmine.createSpyObj('IngredientDataServices',['getProductByDescription','getProductByFoodgroup','deleteIngredient'])
const calculationService=jasmine.createSpyObj('CalculationService',['gramsCalculation'])
  beforeEach(async(() => {
    TestBed.configureTestingModule({
        providers: [
            {
                provide: IngredientDataServices,
                useValue: ingredientDataServices,
            },
            {
                provide: CalculationService,
                useValue: calculationService,
            },
        ],
      declarations: [ IngredientComponent ],
      imports: [
        FormsModule,
        TypeaheadModule.forRoot(),
        NgbTypeaheadModule,
        NgbModule 
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IngredientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('findProduct should call getProductByDescription', () => {
    const selected="Butter, salted"
    const selectedMeasure="testMeasure"
    fixture = TestBed.createComponent(IngredientComponent);
    const app = fixture.debugElement.componentInstance
    const product={
        code:1001,
        description: "Butter, salted",
        foodGroup: "Dairy and Egg Products",
        measures:[],
        nutrition:[]
    }
    if (typeof Array.prototype.find !== 'function') {
        Array.prototype.find = function(iterator) {
            var list = Object(this);
            var length = list.length >>> 0;
            var thisArg = arguments[1];
            var value;
            for (var i = 0; i < length; i++) {
                value = list[i];
                if (iterator.call(thisArg, value, i, list)) {
                    return value;
                }
            }
            return undefined;    
        };
    }
    ingredientDataServices.getProductByDescription.and.returnValue(of(product))
    app.findPorduct(selected)
    expect(ingredientDataServices.getProductByDescription).toHaveBeenCalled()
  });
  it('getProductByDescription should be called with correct parameters', () => {
    const selected="Butter, salted"
    const selectedMeasure="testMeasure"
    fixture = TestBed.createComponent(IngredientComponent);
    const app = fixture.debugElement.componentInstance
    const product={
        code:1001,
        description: "Butter, salted",
        foodGroup: "Dairy and Egg Products",
        measures:[],
        nutrition:[]
    }
    if (typeof Array.prototype.find !== 'function') {
        Array.prototype.find = function(iterator) {
            var list = Object(this);
            var length = list.length >>> 0;
            var thisArg = arguments[1];
            var value;
            for (var i = 0; i < length; i++) {
                value = list[i];
                if (iterator.call(thisArg, value, i, list)) {
                    return value;
                }
            }
            return undefined;    
        };
    }
    ingredientDataServices.getProductByDescription.and.returnValue(of(product))
    app.findPorduct(selected)
    expect(ingredientDataServices.getProductByDescription).toHaveBeenCalled()
    expect(ingredientDataServices.getProductByDescription).toHaveBeenCalledWith(selected)
  });

  it('foodGroupfilter should call getProductByFoodgroup', () => {
    let foodGroups=[]
    const group="Dairy and Egg Products"
    fixture = TestBed.createComponent(IngredientComponent);
    const app = fixture.debugElement.componentInstance
    ingredientDataServices.getProductByFoodgroup.and.returnValue(of(foodGroups))
    app.foodGroupfilter(group)
    expect(ingredientDataServices.getProductByFoodgroup).toHaveBeenCalled()
  });
  it('foodGroupfilter should be called with correct parameters', () => {
    let foodGroups=[]
    const group="Dairy and Egg Products"
    fixture = TestBed.createComponent(IngredientComponent);
    const app = fixture.debugElement.componentInstance
    ingredientDataServices.getProductByFoodgroup.and.returnValue(of(foodGroups))
    app.foodGroupfilter(group)
    expect(ingredientDataServices.getProductByFoodgroup).toHaveBeenCalled()
    expect(ingredientDataServices.getProductByFoodgroup).toHaveBeenCalledWith(group)
  });
//   it('calculateGrams should be call gramsCalculation', () => {
//     const value=2
//     const selectedMeasure="cup"
//     const gramsPerMeasure=100
//     const selected="Butter, salted"
//     fixture = TestBed.createComponent(IngredientComponent);
//     const app = fixture.debugElement.componentInstance
    
          
//     calculationService.gramsCalculation.and.returnValue(of(200))
//     app.calculateGrams(value,selectedMeasure)
//     expect(calculationService.calculateGrams).toHaveBeenCalled()


//   });



  
});
