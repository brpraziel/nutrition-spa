import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import {Observable} from 'rxjs';
import {debounceTime, distinctUntilChanged, map} from 'rxjs/operators';
import { IngredientDataServices } from '../../core/services/ingredient.service';
import { CalculationService } from '../../core/services/calculation.service';
import { products_names_list } from './product-data/product-list';
import Swal from 'sweetalert2';


export interface myinterface {
  removeComponent(index: number);
}
@Component({
  selector: 'app-ingredient',
  templateUrl: './ingredient.component.html',
  styleUrls: ['./ingredient.component.css']
})
export class IngredientComponent implements OnInit {
  names = products_names_list;

  constructor(
    private ingredientDataServices:IngredientDataServices,
    private calculationService:CalculationService
    ) { }

  @Input()
  event

  @Input()
  amount:number

  @Input()
  incrementedValue

  @Input()
  selectedMeasureEdit

  @Input()
  ingreID

  public itemNutrients:any
  public calculatedNutritions:any
  item=false
  item2=false
  public params
  public searchValue
  public nutrientProtein
  public nutrientCarbs
  public nutrientFat
  public nutrientCal
  public index: number;
  public selfRef: IngredientComponent ;
  public selected: string;
  public listOfMeasurments:any
  public model: any;
  public compInteraction: myinterface;
  public exampleFormControlSelect1:any
  public selectedMeasure:string
  filter:boolean=false

  ngOnInit() {
    if(this.event===undefined || this.event==="Search Ingredient"){
      return
    }
    this.findPorduct(this.event,this.selectedMeasureEdit)
  }


  foodGroups=["All FoodGroups","Dairy and Egg Products","Spices and Herbs","Baby Foods","Fats and Oils","Poultry Products","Soups, Sauces, and Gravies",
  "Sausages and Luncheon Meats","Breakfast Cereals","Fruits and Fruit Juices","Pork Products","Vegetables and Vegetable Products","Nut and Seed Products","Beef Products",
  "Beverages","Finfish and Shellfish Products","Legumes and Legume Products","Lamb, Veal, and Game Products","Baked Products","Sweets",
  "Cereal Grains and Pasta","Fast Foods","Meals, Entrees, and Side Dishes","Snacks","American Indian/Alaska Native Foods","Restaurant Foods"]

  search = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      map(term => term.length < 2 ? []
        : this.names.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
        )
        
        
        
       public selectItem(event: any) {
          this.selected=event.item
          this.findPorduct(this.selected)
        }


        public selectItem2(event: any) {
          this.selected=event
           this.findPorduct(event)
         }
      
        
        public findPorduct(selected,selectedMeasure?){
          this.ingredientDataServices.getProductByDescription(selected).subscribe((params)=>{ 
            
            
            this.params=params
            this.listOfMeasurments=params[0].measures
            this.listOfMeasurments.unshift({
              gramsPerMeasure: 1,
              measure: "g"})
              
              if(!!selectedMeasure){
              let firstMeasure=this.listOfMeasurments.filter(x=>x.measure===selectedMeasure)
              let index=this.listOfMeasurments.findIndex(x=>x.measure===selectedMeasure)
              this.listOfMeasurments.splice(index,1)
              this.listOfMeasurments.unshift(firstMeasure[0])
              }
          this.itemNutrients=params[0].nutrition[0]
           delete this.itemNutrients.id
          })
          this.incrementedValue=this.amount
        }
        

       public foodGroupfilter(goup){
          this.filter=true
          if(goup==="All FoodGroups"||goup===undefined){
            this.names = products_names_list;
            return
          }
        this.ingredientDataServices.getProductByFoodgroup(goup).subscribe(params=>{
          
          let description=params.map(x=>{return x.description})
          this.names=description
        })
}


        public increments(value){
          this.incrementedValue=value
        }

        public calculateGrams(value,selectedMeasure){
          if(selectedMeasure===undefined){
            selectedMeasure='g'
          }
          const measureToUse=this.listOfMeasurments.filter(function(measure) {
            return measure.measure == selectedMeasure;
          });
            const gramsOfMeasure=measureToUse[0].gramsPerMeasure
          this.amount=+this.calculationService.gramsCalculation(+gramsOfMeasure,+value).toFixed(2)
          this.calculateNutrition()
        }



        filterChanged(selectedValue:string){
        this.selectedMeasure=selectedValue
         }


   public calculateNutrition(){
    const amount=+this.amount

    let calculatedNutrition=Object.values(this.itemNutrients).map((element:any) => {
      element.value=+element.value
      return element
     });


    this.calculatedNutritions=calculatedNutrition
    this.calculatedNutritions=this.calculatedNutritions
    this.nutrientCal=+this.calculatedNutritions[3].value
    this.nutrientProtein=(((+this.calculatedNutritions[0].value*4)/this.nutrientCal)*100).toFixed(1)
    this.nutrientCarbs=+this.calculatedNutritions[2].value*4
    this.nutrientFat=(((+this.calculatedNutritions[1].value*9)/this.nutrientCal)*100).toFixed(1)
    this.calculatedNutritions=this.calculatedNutritions.splice(3)
   }
  
   public removeMe(index) {
    this.compInteraction.removeComponent(index)
  }

  public removeField(index){
    Swal.fire ({
      title: 'Are you sure?',
      text: "Do you want to remove this field?",
      type: 'warning', 
      showCancelButton: true, 
      confirmButtonColor: '#00B96F',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, remove!' 
    }).then((result) => {
      if (result.value) {
        this.removeMe(index)
        Swal.fire(
          'Deleted!',
          'Your Ingredient has been deleted.',
          'success'
        )
      }
    })

  }
  
}
