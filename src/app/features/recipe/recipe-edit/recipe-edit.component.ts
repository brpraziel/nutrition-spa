import { Component, ComponentFactory, ComponentFactoryResolver, ComponentRef, OnDestroy, OnInit, ViewChild, ViewContainerRef, Type, NgZone, AfterViewInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DndDropEvent, DropEffect } from 'ngx-drag-drop';
import { field, value } from 'src/global.model';
import Swal from 'sweetalert2';
import { IngredientComponent } from '../../ingredient/ingredient.component';
import { RecipeService } from 'src/app/core/services/recipe.service';
import { Ingredient } from 'src/app/common/interfaces/ingredient';
import { SubrecipeComponent } from '../../subrecipe/subrecipe.component';
import { Recipe } from 'src/app/common/interfaces/recipe';
import { NutritionCalculationService } from 'src/app/core/services/nutrition-calculation.service';
import { IngredientDataServices } from 'src/app/core/services/ingredient.service';

import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";

import am4themes_animated from "@amcharts/amcharts4/themes/animated";
@Component({
  selector: 'app-recipe-edit',
  templateUrl: './recipe-edit.component.html',
  styleUrls: ['./recipe-edit.component.css']
})
export class RecipeEditComponent implements OnInit,OnDestroy,AfterViewInit {
  value:value={
    label:"",
    value:""
  }
  receipeName:string
  recipeNutrition
  private chart: am4charts.PieChart;
  recipeIngredients
  success = false;
  selected: string;
  totalWeight:number
  index: number = 0;
  subrecipeIndex: number = 0;
  recipeToEdit
  recipeToEditID
  recipeDetails
  nutritionSummary
  nutritionFull
  ingred
  ingredID
  usedSubrecipes
  ingredientViews
  subrecipes
  recipeDescription:string
  courseOption

@ViewChild('ing',{read:ViewContainerRef,static:false}) container:ViewContainerRef
@ViewChild('subrecipe',{read:ViewContainerRef,static:false}) subrecipeContainer:ViewContainerRef

subrecipeComponentRef:ComponentRef<SubrecipeComponent>
fooIngredientComponent=IngredientComponent
fooSubrecipeComponent=SubrecipeComponent
componentRef:ComponentRef<IngredientComponent>
subrecipeComponents=[]
components = []

createSubrecipeComponent(name,amount) {
  //this.container.clear();  
  const subrecipefactory: ComponentFactory<any> = this.resolver.resolveComponentFactory(SubrecipeComponent);
  this.subrecipeComponentRef = this.subrecipeContainer.createComponent(subrecipefactory);
  this.subrecipeComponents.push(this.subrecipeComponentRef);

  let currentSubrecipeComponent = this.subrecipeComponentRef.instance;

  currentSubrecipeComponent.selfRef1 = currentSubrecipeComponent;
  currentSubrecipeComponent.compInteraction = this
  currentSubrecipeComponent.subrecipeIndex = ++this.subrecipeIndex;
  currentSubrecipeComponent.incrementedValue=amount
  currentSubrecipeComponent.amount=amount
  currentSubrecipeComponent.name=name
}

removeSubrecipeComponent(subrecipeIndex1) {

  if (this.subrecipeContainer.length < 1)
      return;
      let SubrecipeComponentRef = this.subrecipeComponents.filter(x => x.instance.subrecipeIndex == subrecipeIndex1 )[0];
      
    let subcomponent: SubrecipeComponent = <SubrecipeComponent>SubrecipeComponentRef.instance;
    let vcrIndex1: number = this.subrecipeContainer.indexOf(SubrecipeComponentRef)
    this.subrecipeContainer.remove(vcrIndex1);
    this.subrecipeComponents = this.subrecipeComponents.filter(x => x.instance.subrecipeIndex !== this.subrecipeIndex);
  }

createComponent(amount,selected,selectedMeasure,id?) {
  const factory: ComponentFactory<any> = this.resolver.resolveComponentFactory(IngredientComponent);
  this.componentRef = this.container.createComponent(factory);
  this.components.push(this.componentRef);
  let currentComponent = this.componentRef.instance;
    currentComponent.selfRef = currentComponent;
    currentComponent.index = ++this.index;
    currentComponent.compInteraction = this
    currentComponent.amount=amount
    currentComponent.selectedMeasureEdit=selectedMeasure
    currentComponent.ingreID=id
    if(selected===undefined){
      selected="Search Ingredient"
    }
    currentComponent.event=selected
}

removeComponent(index) {
  if (this.container.length < 1)
      return;
    let componentRef = this.components.filter(x => x.instance.index == index)[0];
    let component: IngredientComponent = <IngredientComponent>componentRef.instance;
    let vcrIndex: number = this.container.indexOf(componentRef)
    let id =component.ingreID
    this.ingredientDataServices.deleteIngredient(id).subscribe(params=>{}
    )
     this.container.remove(vcrIndex);
    this.components = this.components.filter(x => x.instance.index !== index);
  }
  fieldModels:Array<field>=[
    {
      "type": "text",
      "icon": "fa fa-pie-chart",
      "label": "Ingredient",
      "description": "Enter your Ingrediant",
      "placeholder": "Enter your Ingrediant",
      "className": Math.random(),
      "subtype": "text",
      "regex" : "",
      "handle":true,
    },
    {
      "type": "email",
      "icon": "fa fa-list-alt",  
      "label": "Recipe",
    },
    {
      "type": "textarea",
      "icon":"fa-text-width",
      "label": "Description" ,
      "description": "not",
    },
    {
      "type": "radio",
      "icon":"fa-list-ul",
      "label": "Courses",
      "description": "Radio boxes",
      "values":[
        {
          "label": "Dinner",
          "value": "1"
        },
        {
          "label": "Launch",
          "value": "2"
        },
        {
          "label": "Pasta",
          "value": "3"
        },
        {
          "label": "Breads,Muffin,Scones",
          "value": "4"
        },
        {
          "label": "Salad",
          "value": "5"
        },
        {
          "label": "Breakfast",
          "value": "6"
        },
        {
          "label": "Soup",
          "value": "7"
        },
        {
          "label": "Desserts",
          "value": "8"
        },
        {
          "label": "Pizza",
          "value": "9"
        },
        {
          "label": "Sandwiches,Wraps",
          "value": "10"
        },
        {
          "label": "Snacks",
          "value": "11"
        },
        {
          "label": "Side_Dishes",
          "value": "12"
        },
        {
          "label": "Dips,Salad_dresings,Salsas",
          "value": "13"
        },
        {
          "label": "Drinks",
          "value": "14"
        },
        {
          "label": "Main_Cousres",
          "value": "15"
        },
        {
          "label": "No Category",
          "value": "16"
        }
      ]
    },
    {
      "type": "file",
      "icon":"fa-file",
      "label": "Upload Image",
      "className": "form-control",
      "subtype": "file",
      "description": "not",
    },
  ];

  modelFields:Array<field>=[];
  model:any = {
    name:'EDIT',
    description:'Recipe Description...',
    theme:{
      bgColor:"ffffff",
      textColor:"555555",
      bannerImage:""
    },
    attributes:this.modelFields
  };

  report = false;
  reports:any = [];
  constructor(
    private route:ActivatedRoute,
    private resolver: ComponentFactoryResolver,
    private recipeService:RecipeService,
    private readonly nutriCalc: NutritionCalculationService,
    private readonly ingredientDataServices:IngredientDataServices,
    private zone: NgZone,
  ) { }

  
  ngAfterViewInit(){
    this.route.queryParams.subscribe(params => {
      if(Object.entries(params).length === 0){
        return
      }
      let objParams=JSON.parse(params.add)
      let test=new Array(objParams)
    test[0].forEach(e => {
              let name=''
            this.ingredientDataServices.getProductByDescription(e.productName).subscribe(params=>{
              name=params[1].description
              
              this.createComponent(e.amount,name,e.selectedMeasure,params[0].id)
            })
  })
    })
  }
  change=true
  ngOnInit() {
    this.change=true
    this.route.queryParams.subscribe(params => {
      if(Object.entries(params).length === 0){
        this.change=true
        return
      }
      this.change=false
      
      
      let objParams=JSON.parse(params.add)
      let test=new Array(objParams)
    test[0].forEach(e => {
           // this.createComponent(e.amount,e.productName,e.selectedMeasure)
            
      //return this.createComponent(e.amount,e.productName,e.selectedMeasure)
    
  })
    
    })
    
    if(this.change){
    this.route.data.subscribe(
      (data) => {
        
        this.recipeDetails = data.recipe_details[0];
        this.ingred=this.recipeDetails.ingredients
        this.recipeService.getSingleRecipeByTitle(`${this.recipeDetails.title}`).subscribe(params=>{
          this.recipeDescription=params[0].information
          this.recipeToEditID=params[0].id
          const falseIngrediaents= params[0].ingredients.filter(x=>{return x.isArchived===false})
          falseIngrediaents.forEach(e => {
            
            return this.createComponent(e.amount,e.product.description,e.selectedMeasure,e.id)
          
        })
        this.usedSubrecipes=params[0].usedSubrecipesIds
        params[0].usedSubrecipesIds.forEach(s=>{this.createSubrecipeComponent(s.subrecipeName,s.amount)})
      });
     })
    this.recipeToEdit=this.recipeDetails.title
    this.courseOption=this.recipeDetails.category
    }
  }

  onDragStart(event:DragEvent) {
    console.log("drag started", JSON.stringify(event, null, 2));
  }
  
  onDragEnd(event:DragEvent) {
    console.log("drag ended", JSON.stringify(event, null, 2));
  }
  
  onDraggableCopied(event:DragEvent) {
    console.log("draggable copied", JSON.stringify(event, null, 2));
  }
  
  onDraggableLinked(event:DragEvent) {
    console.log("draggable linked", JSON.stringify(event, null, 2));
  }
    
   onDragged( item:any, list:any[], effect:DropEffect ) {
    if( effect === "move" ) {
      const index = list.indexOf( item );
      list.splice( index, 1 );
    }
  }
      
  onDragCanceled(event:DragEvent) {
    console.log("drag cancelled", JSON.stringify(event, null, 2));
  }
  
  onDragover(event:DragEvent) {
    console.log("dragover", JSON.stringify(event, null, 2));
  }
  
  onDrop( event:DndDropEvent, list?:any[] ) {
    if( list && (event.dropEffect === "copy" || event.dropEffect === "move") ) {
      
      if(event.dropEffect === "copy")
      event.data.name = event.data.type+'-'+new Date().getTime();
      let index = event.index;
      if( typeof index === "undefined" ) {
        index = list.length;
      }
      list.splice( index, 0, event.data );
    }
  }

  addValue(values){
    values.push(this.value);
    this.value={label:"",value:""};
  } 
  
  removeField(i){
    Swal.fire ({
      title: 'Are you sure?',
      text: "Do you want to remove this field?",
      type: 'warning', 
      showCancelButton: true, 
      confirmButtonColor: '#00B96F',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, remove!' 
    }).then((result) => {
      if (result.value) {
        this.model.attributes.splice(i,1);
      }
    });
  }

  initReport(){
    this.report = true; 
    let input = {
      id:this.model._id
    }
  }

  
  toggleValue(item){
    item.selected = !item.selected;
  }

  submit(){
    





    this.ingredientViews=this.components
    const ingrediantsToReturn=this.ingredientViews.map(x=>{
      let name=x.instance.selected
      let productAmount=x.instance.incrementedValue
      if(productAmount===undefined){
        productAmount=x.instance.amount
      }
      if(name===undefined){
        name=x.instance.event
      }
      let measure=x.instance.selectedMeasure
      if(measure===undefined){
        measure=x.instance.selectedMeasureEdit
      }
      
      let obj:Ingredient={
        id:x.instance.ingreID,
        productName:name,
        amount:+productAmount,
        selectedMeasure:measure}
      return obj
    })
    
    this.subrecipes=this.subrecipeComponents.map(x=>{
      let name=x.instance.receipeToReturn
      if(!name){
        name=x.instance.name
      }
      let amount=x.instance.incrementedValue
      if(!amount){
        amount=x.instance.amount
      }
      let subrecipeObj={
          subrecipeName:name,
          amount:amount
      }
      return subrecipeObj
    })
    const recipeIngredients=ingrediantsToReturn
    let recipeCategory=this.courseOption
    if(recipeCategory===undefined){
      recipeCategory="1"
    }
    let recipeSubreceryes=this.subrecipes
    if(!recipeSubreceryes){
      recipeSubreceryes=[]
    }
    
    
    if(!this.change){
      
      
      let newones=recipeIngredients.map(x=>{if(!x.selectedMeasure){
        delete x.id
        x.selectedMeasure=x.selectedMeasureEdit
        return x 
      }})
      const returnRecipe={
        title:this.receipeName,
        ingredients:recipeIngredients,
        usedSubrecipesIds:newones,
        category:+recipeCategory,
        information:this.recipeDescription
      }
      
      this.recipeService.createRecipe(returnRecipe).subscribe((params) => {
      }, (err: any) => {
        Swal.fire({
          type: 'error',
          title: 'Oops...',
          text: 'Duplicate Recipe Name OR unproper ingredient'
  
        })
      })
      return
    }
    const createRecipe={
      recipiID:this.recipeDetails.id,
      ingredients:recipeIngredients,
      usedSubrecipesIds:recipeSubreceryes,
      category:+recipeCategory,
      description:this.recipeDescription
    }
    Swal.fire({
      position: 'top-end',
      type: 'success',
      title: 'Your Recipe has been saved',
      showConfirmButton: false,
      timer: 1500
    })
     this.recipeService.editRecipe(this.recipeToEditID,createRecipe).subscribe(params=>{
     },(err: any) =>{
      
     }
      )
  }
  ngOnDestroy() {
    this.zone.runOutsideAngular(() => { 
      if (this.chart) {
        this.chart.dispose();
      }
    });
  }

  analyseRecipe() {
    let cal = this.components.map(x => {
      let instance = Object.values(x.instance.itemNutrients)
      const amount = x.instance.amount
      let newinstance = instance.map((x: any) => {
        return x.value / 100 * amount
      })
      return newinstance
    })
    let cal2 = cal.reduce(function (array1, array2) {
      return array1.map(function (value, index) {
        return value + array2[index];
      });
    });

    this.recipeDetails = {
      ingredients: {
        product: {
          ingredients: this.recipeIngredients
        }
      }
    }

    const ingrediantsToAnalyseCal = this.components.map(x => {
      const nutrientCal = +x.instance.itemNutrients.ENERC_KCAL.value
      return nutrientCal
    }).reduce((a, b) => +a + +b)
    
    const ingrediantsToAnalysePro = this.components.map(x => {
      const nutrientProtein = +x.instance.itemNutrients.PROCNT.value
      return nutrientProtein
    }).reduce((a, b) => +a + +b)

    const ingrediantsToAnalyseCarbs = this.components.map(x => {
      const nutrientCarbs = x.instance.itemNutrients.CHOCDF.value
      return nutrientCarbs
    }).reduce((a, b) => +a + +b)

    const ingrediantsToAnalyseFat = this.components.map(x => {
      const nutrientFat = x.instance.itemNutrients.FAT.value
      return nutrientFat
    }).reduce((a, b) => +a + +b)

    this.recipeNutrition = [{
        nutrient: "kcal",
        value: +ingrediantsToAnalyseCal
      },
      {
        nutrient: "carbs",
        value: +ingrediantsToAnalyseCarbs
      },
      {
        nutrient: "fat",
        value: +ingrediantsToAnalyseFat
      },
      {
        nutrient: "proteins",
        value: +ingrediantsToAnalysePro
      },
    ]
    this.zone.runOutsideAngular(() => {
      let chart = am4core.create("chart-1", am4charts.PieChart);
      chart.hiddenState.properties.opacity = 0; 
      chart.paddingTop = 0;
      let data = this.nutritionSummary;
      chart.data = [{
          "nutrient": "carbs",
          "value": +ingrediantsToAnalyseCarbs
        },
        {
          "nutrient": "fat",
          "value": +ingrediantsToAnalyseFat
        },
        {
          "nutrient": "proteins",
          "value": +ingrediantsToAnalysePro
        }
      ];

      chart.innerRadius = am4core.percent(50);
      let label = chart.seriesContainer.createChild(am4core.Label);
      label.text = "per 100g";
      label.horizontalCenter = "middle";
      label.verticalCenter = "middle";
      label.fontSize = 13;
      let pieSeries = chart.series.push(new am4charts.PieSeries());
      pieSeries.colors.list = [
        am4core.color("#628800"),
        am4core.color("#aed407"),
        am4core.color("#C8EE21"),
      ];
      pieSeries.ticks.template.disabled = true;
      pieSeries.labels.template.disabled = true;
      pieSeries.dataFields.category = "nutrient";
      pieSeries.dataFields.value = "value";

      pieSeries.hiddenState.properties.startAngle = 90;
      pieSeries.hiddenState.properties.endAngle = 90;

      this.chart = chart;
    });

    this.zone.runOutsideAngular(() => {
      let chart = am4core.create("chart-2", am4charts.PieChart);

      chart.hiddenState.properties.opacity = 0; 
      chart.paddingTop = 0;

      chart.data = [{
          "nutrient": "sugar",
          "value": cal2[4]
        },
        {
          "nutrient": "fiber",
          "value": cal2[5]
        },
        {
          "nutrient": "calcium",
          "value": cal2[6]
        },
        {
          "nutrient": "iron",
          "value": cal2[7]
        },
        {
          "nutrient": "phosphorus",
          "value": cal2[8]
        },
        {
          "nutrient": "potassium",
          "value": cal2[9]
        },
        {
          "nutrient": "sodium",
          "value": cal2[10]
        },
        {
          "nutrient": "vitaminA",
          "value": cal2[11]
        },
        {
          "nutrient": "vitaminE",
          "value": cal2[12]
        },
        {
          "nutrient": "vitaminD",
          "value": cal2[13]
        },
        {
          "nutrient": "vitaminC",
          "value": cal2[14]
        },
        {
          "nutrient": "vitaminB12",
          "value": cal2[15]
        },
        {
          "nutrient": "folicAcid",
          "value": cal2[16]
        },
        {
          "nutrient": "cholesterol",
          "value": cal2[17]
        },
      ];
      chart.innerRadius = am4core.percent(50);
      let label = chart.seriesContainer.createChild(am4core.Label);
      label.text = "per 100g";
      label.horizontalCenter = "middle";
      label.verticalCenter = "middle";
      label.fontSize = 13;
      let pieSeries = chart.series.push(new am4charts.PieSeries());
      pieSeries.colors.list = [
        am4core.color("#628800"),
        am4core.color("#aed407"),
        am4core.color("#C8EE21"),
        am4core.color("#628800"),
        am4core.color("#aed407"),
        am4core.color("#C8EE21"),
        am4core.color("#628800"),
        am4core.color("#aed407"),
        am4core.color("#C8EE21"),
        am4core.color("#628800"),
        am4core.color("#aed407"),
        am4core.color("#C8EE21"),
        am4core.color("#628800"),
        am4core.color("#aed407"),
      ];
      pieSeries.ticks.template.disabled = true;
      pieSeries.labels.template.disabled = true;
      pieSeries.dataFields.value = "value";
      pieSeries.dataFields.category = "nutrient";

      pieSeries.hiddenState.properties.startAngle = 90;
      pieSeries.hiddenState.properties.endAngle = 90;

      this.chart = chart;
    });
  }
}
