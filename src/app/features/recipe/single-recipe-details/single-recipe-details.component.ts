import { Component, OnInit, NgZone, AfterViewInit, OnDestroy } from '@angular/core';
import { Recipe } from '../../../common/interfaces/recipe';
import { ActivatedRoute, Router } from '@angular/router';
import { NutritionCalculationService } from '../../../core/services/nutrition-calculation.service';
import { NutritionSummary } from '../../../common/interfaces/nutrition-summary';
import { NutritionFull } from '../../../common/interfaces/nutrition-full';
import { Ingredient } from 'src/app/common/interfaces/ingredient';

import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import { RecipeService } from 'src/app/core/services/recipe.service';
import { SubrecipeDescriptor } from 'src/app/common/interfaces/subrecipe-descriptor';

am4core.useTheme(am4themes_animated);

@Component({
  selector: 'app-single-recipe-details',
  templateUrl: './single-recipe-details.component.html',
  styleUrls: ['./single-recipe-details.component.css']
})
export class SingleRecipeDetailsComponent implements OnInit, AfterViewInit, OnDestroy {
  recipeDetails: Recipe;
  nutritionSummary: NutritionSummary;
  nutritionFull: NutritionFull;
  recipeIngredients: Ingredient[];
  recipeNutrition: {nutrient: string, value: number}[];

  hasSubrecipe = false;
  subrecipeName: string;
  subrecipeInfo: SubrecipeDescriptor | any;

  /* chart */
  private chart: am4charts.PieChart;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly nutriCalc: NutritionCalculationService,
    private readonly recipeService: RecipeService,
    /* chart */
    private zone: NgZone,
    private readonly router: Router,
  ) { }

  ngOnInit() {
    this.route.data.subscribe(
      resolverData => {
        if (resolverData.recipe_details.length > 0) {
          this.recipeDetails = resolverData.recipe_details[0];
          this.recipeIngredients = resolverData.recipe_details[0].ingredients;
        } else {
          return console.log('Error in retrieval of recipe details.');
        }
      },
      error => {
        console.log('An error ocurred while taking data from recipe\'s resolvers.');
      }
    );

    this.nutritionSummary = this.nutriCalc.calculateNutriSummaryForRecipe(this.recipeDetails);
    this.nutritionFull = this.nutriCalc.calculateFullNutritionForRecipe(this.recipeDetails);

    /* formatting data for table view */
    const data = this.nutritionFull;
    this.recipeNutrition = [
      {nutrient: "kcal", value: data.energy},
      {nutrient: "carbs", value: data.carbs},
      {nutrient: "fat", value: data.fat},
      {nutrient: "proteins", value: data.proteins},
      {nutrient: "sugar", value: data.sugar},
      {nutrient: "fiber", value: data.fiber},
      {nutrient: "calcium", value: data.calcium},
      {nutrient: "iron", value: data.iron},
      {nutrient: "phosphorus", value: data.phosphorus},
      {nutrient: "potassium", value: data.potassium},
      {nutrient: "sodium", value: data.sodium},
      {nutrient: "vitamin A", value: data.vitaminA},
      {nutrient: "vitamin E", value: data.vitaminE},
      {nutrient: "vitamin D", value: data.vitaminD},
      {nutrient: "vitamin C", value: data.vitaminC},
      {nutrient: "vitamin B12", value: data.vitaminB12},
      {nutrient: "folic acid", value: data.folicAcid},
      {nutrient: "cholesterol", value: data.cholesterol},
    ];

    // subrecipe
    if (this.recipeDetails.usedSubrecipesIds.length > 0) {
      this.hasSubrecipe = true;
      this.subrecipeInfo = this.recipeDetails.usedSubrecipesIds.map(
        element => {
          return element;
        }
      );
      this.subrecipeName = this.subrecipeInfo.map(
        element => element.subrecipeName
      );
    }

  }

  /* charts */
  ngAfterViewInit() {
    /* CHART-1 */
    this.zone.runOutsideAngular(() => {
      let chart = am4core.create('chart-1', am4charts.PieChart);

      chart.hiddenState.properties.opacity = 0; // this creates initial fade-in

      chart.paddingTop = 0;
      let data = this.nutritionSummary;
      chart.data = [
        {"nutrient": "carbs", "value": data.carbs},
        {"nutrient": "fat", "value": data.fat},
        {"nutrient": "proteins", "value": data.proteins}
      ];

      //  Add label
      chart.innerRadius = am4core.percent(50);
      let label = chart.seriesContainer.createChild(am4core.Label);
      label.text = "per 100g";
      label.horizontalCenter = "middle";
      label.verticalCenter = "middle";
      label.fontSize = 13;

      // Add and configure Series
      let pieSeries = chart.series.push(new am4charts.PieSeries());
      pieSeries.colors.list = [
        am4core.color("#628800"),
        am4core.color("#aed407"),
        am4core.color("#C8EE21"),
      ];
      pieSeries.ticks.template.disabled = true;
      pieSeries.labels.template.disabled = true;
      pieSeries.dataFields.category = "nutrient";
      pieSeries.dataFields.value = "value";

      pieSeries.hiddenState.properties.startAngle = 90;
      pieSeries.hiddenState.properties.endAngle = 90;

      this.chart = chart;
    });

    /* CHART-2 */
    this.zone.runOutsideAngular(() => {
      let chart = am4core.create("chart-2", am4charts.PieChart);

      chart.hiddenState.properties.opacity = 0; // this creates initial fade-in

      chart.paddingTop = 0;
      let data = this.nutritionFull;
      chart.data = [
        {"nutrient": "sugar", "value": data.sugar},
        {"nutrient": "fiber", "value": data.fiber},
        {"nutrient": "calcium", "value": data.calcium},
        {"nutrient": "iron", "value": data.iron},
        {"nutrient": "phosphorus", "value": data.phosphorus},
        {"nutrient": "potassium", "value": data.potassium},
        {"nutrient": "sodium", "value": data.sodium},
        {"nutrient": "vitaminA", "value": data.vitaminA},
        {"nutrient": "vitaminE", "value": data.vitaminE},
        {"nutrient": "vitaminD", "value": data.vitaminD},
        {"nutrient": "vitaminC", "value": data.vitaminC},
        {"nutrient": "vitaminB12", "value": data.vitaminB12},
        {"nutrient": "folicAcid", "value": data.folicAcid},
        {"nutrient": "cholesterol", "value": data.cholesterol},
      ];

      // Add label
      chart.innerRadius = am4core.percent(50);
      let label = chart.seriesContainer.createChild(am4core.Label);
      label.text = "per 100g";
      label.horizontalCenter = "middle";
      label.verticalCenter = "middle";
      label.fontSize = 13;

      // Add and configure Series
      let pieSeries = chart.series.push(new am4charts.PieSeries());
      pieSeries.colors.list = [
        am4core.color("#628800"),
        am4core.color("#aed407"),
        am4core.color("#C8EE21"),
        am4core.color("#628800"),
        am4core.color("#aed407"),
        am4core.color("#C8EE21"),
        am4core.color("#628800"),
        am4core.color("#aed407"),
        am4core.color("#C8EE21"),
        am4core.color("#628800"),
        am4core.color("#aed407"),
        am4core.color("#C8EE21"),
        am4core.color("#628800"),
        am4core.color("#aed407"),
      ];
      pieSeries.ticks.template.disabled = true;
      pieSeries.labels.template.disabled = true;
      pieSeries.dataFields.value = "value";
      pieSeries.dataFields.category = "nutrient";

      pieSeries.hiddenState.properties.startAngle = 90;
      pieSeries.hiddenState.properties.endAngle = 90;

      this.chart = chart;
    });
  }

  reRoute() {
    const url = `/(print:print/printer/${this.recipeDetails.title})`;
    this.router.navigateByUrl(url);
  }

  ngOnDestroy() {
    this.zone.runOutsideAngular(() => {
      if (this.chart) {
        this.chart.dispose();
      }
    });
  }

}
