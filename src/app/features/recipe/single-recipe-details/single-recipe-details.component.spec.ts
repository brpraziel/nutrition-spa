import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { SingleRecipeDetailsComponent } from './single-recipe-details.component';
import { of } from 'rxjs';
import { SharedModule } from 'src/app/shared/shared.module';
import { ActivatedRoute, Router, RouterModule } from '@angular/router';
import { NgZone } from '@angular/core';
import { NutritionCalculationService } from 'src/app/core/services/nutrition-calculation.service';
import { SingleIngredientComponent } from './single-ingredient/single-ingredient.component';


describe('SingleRecipeDetailsComponent', () => {
  let component: SingleRecipeDetailsComponent;
  let fixture: ComponentFixture<SingleRecipeDetailsComponent>;
  // tslint:disable-next-line: max-line-length
  const nutriCalc = jasmine.createSpyObj('NutritionCalculationService', ['calculateNutriSummaryForRecipe', 'calculateFullNutritionForRecipe']);
  const router = jasmine.createSpyObj('Router', ['navigateByUrl']);
  const zone = jasmine.createSpyObj('NgZone', ['runOutsideAngular']);
  const route = jasmine.createSpyObj('ActivatedRoute', ['']);

  const eggProduct = {
      description: 'egg',
  };

  const eggIngredient = {
      amount: 3,
      product: eggProduct,
      selectedMeasure: 'medium',
  };

  route.data = of({
    recipe_details: [
        {
            title: 'Pancakes',
            ingredients: [eggIngredient],
            overallNutrition: 250,
            information: 'Easy recipe',
        }
    ]
  });

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        SingleRecipeDetailsComponent,
        SingleIngredientComponent,
      ],
      imports: [
        SharedModule,
       // RouterTestingModule.withRoutes([]),
        // SidebarModule.forRoot(),
        // HttpClientModule,
        RouterModule,
        // NgbModule,
      ],
      providers: [
        {
            provide: NgZone,
            useValue: zone,
        },
        {
            provide: ActivatedRoute,
            useValue: route,
        },
        {
            provide: Router,
            useValue: router,
        },
        {
            provide: NutritionCalculationService,
            useValue: nutriCalc,
        },
      ]
    })
    .compileComponents();
  }));

  beforeEach(async () => {
    fixture = TestBed.createComponent(SingleRecipeDetailsComponent);
    component = fixture.componentInstance;
    await fixture.detectChanges();
  });

  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });

});
