import { Component, OnInit, Input } from '@angular/core';
import { Ingredient } from 'src/app/common/interfaces/ingredient';
import { Measure } from 'src/app/common/interfaces/measure';
import { Nutrition } from 'src/app/common/interfaces/nutrition';
import { NutritionFull } from 'src/app/common/interfaces/nutrition-full';


@Component({
  selector: 'app-single-ingredient',
  templateUrl: './single-ingredient.component.html',
  styleUrls: ['./single-ingredient.component.css']
})
export class SingleIngredientComponent implements OnInit {
  public toggle = false;

  ingredientNutriFull: NutritionFull = {
    energy: 0,
    proteins: 0,
    carbs: 0,
    fat: 0,
    sugar: 0,
    fiber: 0,
    calcium: 0,
    iron: 0,
    phosphorus: 0,
    potassium: 0,
    sodium: 0,
    vitaminA: 0,
    vitaminE: 0,
    vitaminD: 0,
    vitaminC: 0,
    vitaminB12: 0,
    folicAcid: 0,
    cholesterol: 0,
  };
  formattedIngrNutrition: any[] = [];

  @Input()
  ingredient: Ingredient;
  @Input()
  index: number;

  constructor(
  ) { }

  ngOnInit() {
    const ingredientAmount: number = this.ingredient.amount;
    const selectedMeasure: string = this.ingredient.selectedMeasure;    // todo : transform to base | grams
    const productMeasures: Measure[] = this.ingredient.product.measures;
    // tslint:disable-next-line: max-line-length
    // const selectedMeasurePerHundredGrams: number = NutritionCalculationService.getSelectedMeasureInGrams(selectedMeasure, productMeasures);
    const measure = 1;

    const productNutrition: Nutrition = this.ingredient.product.nutrition[0];

    const kcalVal = productNutrition.ENERC_KCAL.value;  // in kcal - data per 100g
    const proteinVal = productNutrition.PROCNT.value;
    const carbsVal = productNutrition.CHOCDF.value;
    const fatVal = productNutrition.FAT.value;
    const sugarVal = productNutrition.SUGAR.value;
    const fiberVal = productNutrition.FIBTG.value;
    const calciumVal = productNutrition.CA.value;
    const ironVal = productNutrition.FE.value;
    const phosphorusVal = productNutrition.P.value;
    const potassiumVal = productNutrition.K.value;
    const sodiumVal = productNutrition.NA.value;
    const vitaminAVal = productNutrition.VITA_IU.value;
    const vitaminEVal = productNutrition.TOCPHA.value;
    const vitaminDVal = productNutrition.VITD.value;
    const vitaminCVal = productNutrition.VITC.value;
    const vitaminB12Val = productNutrition.VITB12.value;
    const folicAcidVal = productNutrition.FOLAC.value;
    const cholesterolVal = productNutrition.CHOLE.value;

    this.ingredientNutriFull.energy += kcalVal / 100 * (ingredientAmount * measure);
    this.ingredientNutriFull.proteins += proteinVal / 100 * (ingredientAmount * measure);
    this.ingredientNutriFull.carbs += carbsVal / 100 * (ingredientAmount * measure);
    this.ingredientNutriFull.fat += fatVal / 100 * (ingredientAmount * measure);
    this.ingredientNutriFull.sugar += sugarVal / 100 * (ingredientAmount * measure);
    this.ingredientNutriFull.fiber += fiberVal / 100 * (ingredientAmount * measure);
    this.ingredientNutriFull.calcium += calciumVal / 100 * (ingredientAmount * measure);
    this.ingredientNutriFull.iron += ironVal / 100 * (ingredientAmount * measure);
    this.ingredientNutriFull.phosphorus += phosphorusVal / 100 * (ingredientAmount * measure);
    this.ingredientNutriFull.potassium += potassiumVal / 100 * (ingredientAmount * measure);
    this.ingredientNutriFull.sodium += sodiumVal / 100 * (ingredientAmount * measure);
    this.ingredientNutriFull.vitaminA += vitaminAVal / 100 * (ingredientAmount * measure);
    this.ingredientNutriFull.vitaminE += vitaminEVal / 100 * (ingredientAmount * measure);
    this.ingredientNutriFull.vitaminD += vitaminDVal / 100 * (ingredientAmount * measure);
    this.ingredientNutriFull.vitaminC += vitaminCVal / 100 * (ingredientAmount * measure);
    this.ingredientNutriFull.vitaminB12 += vitaminB12Val / 100 * (ingredientAmount * measure);
    this.ingredientNutriFull.folicAcid += folicAcidVal / 100 * (ingredientAmount * measure);
    this.ingredientNutriFull.cholesterol += cholesterolVal / 100 * (ingredientAmount * measure);

    const data = this.ingredientNutriFull;
    data.energy > 0 ? this.formattedIngrNutrition.push({nutrient: "kcal", value: data.energy}) : '';
    data.carbs > 0 ? this.formattedIngrNutrition.push({nutrient: "carbs", value: data.carbs}) : '';
    data.fat > 0 ? this.formattedIngrNutrition.push({nutrient: "fat", value: data.fat}) : '',
    data.proteins > 0 ? this.formattedIngrNutrition.push({nutrient: "proteins", value: data.proteins}) : '';
    data.sugar > 0 ? this.formattedIngrNutrition.push({nutrient: "sugar", value: data.sugar}) : '';
    data.fiber > 0 ? this.formattedIngrNutrition.push({nutrient: "fiber", value: data.fiber}) : '';
    data.calcium > 0 ? this.formattedIngrNutrition.push({nutrient: "calcium", value: data.calcium}) : '';
    data.iron > 0 ? this.formattedIngrNutrition.push({nutrient: "iron", value: data.iron}) : '';
    data.phosphorus > 0 ? this.formattedIngrNutrition.push({nutrient: "phosphorus", value: data.phosphorus}) : '';
    data.potassium > 0 ? this.formattedIngrNutrition.push({nutrient: "potassium", value: data.potassium}) : '';
    data.sodium > 0 ? this.formattedIngrNutrition.push({nutrient: "sodium", value: data.sodium}) : '';
    data.vitaminA > 0 ? this.formattedIngrNutrition.push({nutrient: "vitamin A", value: data.vitaminA}) : '';
    data.vitaminE > 0 ? this.formattedIngrNutrition.push({nutrient: "vitamin E", value: data.vitaminE}) : '';
    data.vitaminD > 0 ? this.formattedIngrNutrition.push({nutrient: "vitamin D", value: data.vitaminD}) : '';
    data.vitaminC > 0 ? this.formattedIngrNutrition.push({nutrient: "vitamin C", value: data.vitaminC}) : '';
    data.vitaminB12 > 0 ? this.formattedIngrNutrition.push({nutrient: "vitamin B12", value: data.vitaminB12}) : '';
    data.folicAcid > 0 ? this.formattedIngrNutrition.push({nutrient: "folic acid", value: data.folicAcid}) : '';
    data.cholesterol > 0 ? this.formattedIngrNutrition.push({nutrient: "cholesterol", value: data.cholesterol}) : '';
  }

  toggleView() {
    this.toggle = !this.toggle;
  }
}
