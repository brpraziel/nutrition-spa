import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Recipe } from '../../../../app/common/interfaces/recipe';
import { ActivatedRoute } from '@angular/router';
import { RecipeService } from 'src/app/core/services/recipe.service';

@Component({
  selector: 'app-recipe-viewer',
  templateUrl: './recipe-viewer.component.html',
  styleUrls: ['./recipe-viewer.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RecipeViewerComponent implements OnInit {
  // 'p' is the loaded (start) page of the pagination
  //  the library we use needs it written exactly like that ('p'), please do not change to 'page', etc.
  p: number = 1;
  allRecipes: Recipe[];
  isAscending: boolean = true;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly recipeService: RecipeService,
  ) {}

  ngOnInit() {
    this.route.data.subscribe(
      data => {
        // load all recipes
        if (data.recipes_list) {
          this.allRecipes = data.recipes_list;
        } else {
          return console.log('Error in retrieval of all recipes.');
        }
      },
      error => {
        console.log('An error ocurred while taking data from recipe\'s resolvers.');
      }
    );
  }

  deleteRecipe(recipeId: string) {
    this.recipeService.archiveRecipeById(recipeId).subscribe();

    const updatedRecipesArr = this.allRecipes.map(
      recipe => {
        if (recipe) {
          if (recipe.id !== recipeId) {
            return recipe;
          }
        }
      }
    );
    this.allRecipes = updatedRecipesArr;
  }

  callSortedRecipes() {
    let sortedRecipes = [];
    if (this.isAscending) {
      sortedRecipes = [...this.allRecipes].sort(
          (recipeOne, recipeTwo): any => {
            return (Number(recipeOne.overallNutrition) - Number(recipeTwo.overallNutrition));
      });
      this.allRecipes = sortedRecipes;
    } else {
      sortedRecipes = [...this.allRecipes].sort(
          (recipeOne, recipeTwo): any => {
            return (Number(recipeTwo.overallNutrition) - Number(recipeOne.overallNutrition));
      });
      this.allRecipes = sortedRecipes;
    }
  }

  toggleSort() {
    if (this.isAscending) {
      this.isAscending = false;
    } else {
      this.isAscending = true;
    }
    this.callSortedRecipes();
  }

}
