import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Recipe } from '../../../../common/interfaces/recipe';
import { Router } from '@angular/router';
import { NutritionSummary } from 'src/app/common/interfaces/nutrition-summary';
import { NutritionCalculationService } from 'src/app/core/services/nutrition-calculation.service';

import Swal from 'sweetalert2';
import { RecipeCategory } from 'src/app/common/enums/category.enum';

@Component({
  selector: 'app-single-recipe-card',
  templateUrl: './single-recipe-card.component.html',
  styleUrls: ['./single-recipe-card.component.css']
})
export class SingleRecipeCardComponent implements OnInit {
  nutritionSummary: NutritionSummary;
  condition = null;
  categoryLabel = RecipeCategory;
  resolvedCategory: string = '';

  @Input()
  recipe: Recipe;

  @Output()
  public triggerDeletion = new EventEmitter<string>();

  constructor(
    private readonly router: Router,
    private readonly nutriCalc: NutritionCalculationService,
  ) { }

  ngOnInit() {
    if (this.recipe) {
      this.nutritionSummary = this.nutriCalc.calculateNutriSummaryForRecipe(this.recipe);
      let catNum = this.recipe.categories;
      this.resolvedCategory = this.getKeyByValue(this.categoryLabel, catNum);
    }
  }

  openDetails() {
    const route = `/recipes/details/${this.recipe.title}`;
    this.router.navigate([route]);
  }

  openDialogue() {
    Swal.fire({
      title: this.recipe.title,
      text: 'You won\'t be able to revert this! Are you sure you want to delete this recipe?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it',
      cancelButtonText: 'No, cancel',
      confirmButtonColor: '#6593f5',
      cancelButtonColor: '#aed407',
      reverseButtons: false
    }).then((result) => {
      if (result.value) {
        this.deleteRecipeTrigger(this.recipe.id);
        Swal.fire(
          'Deleted!',
          'The recipe has been deleted.',
          'error'
        );
      } else if (
        // Read more about handling dismissals
        result.dismiss === Swal.DismissReason.cancel
      ) {
        Swal.fire(
          'Cancelled',
          'The recipe is still available.',
          'success'
        );
      }
    });
  }

  deleteRecipeTrigger(recipeId: string) {
    // console.log('inside single-recipe-card and inside deleteRecipe event; id is ' + recipeId);
    this.triggerDeletion.emit(recipeId);
  }

  toEdit(event) {
    this.router.navigate([`/recipes/edit/${event}`]);
    // console.log(event);
  }

  // helper method for getting the keys for the RecipeCategory enum
  getKeyByValue(object, value: number) {
    return Object.keys(object).find(key => object[key] === value);
  }
}
