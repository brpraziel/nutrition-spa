import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  Resolve,
  RouterStateSnapshot
} from '@angular/router';
import { of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { RecipeService } from '../../core/services/recipe.service';
import { Recipe } from '../../common/interfaces/recipe';

@Injectable({
  providedIn: 'root'
})
export class PrintRecipeResolverService implements Resolve<{ recipe_details: Recipe[] }> {
  recipeTitle: string = '';

  constructor(
    private readonly recipeService: RecipeService,
  ) {}

  public resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    this.recipeTitle = route.paramMap.get('title');
    // console.log('url :title query value ' + this.recipeTitle);

    return this.recipeService.getSingleRecipeByTitle(this.recipeTitle).pipe(
      catchError(res => {
        return of({ recipe_details: [] });
      })
    );
  }
}
