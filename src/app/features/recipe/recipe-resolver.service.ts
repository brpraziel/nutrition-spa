import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  Resolve,
  RouterStateSnapshot
} from '@angular/router';
import { of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Recipe } from '../../common/interfaces/recipe';
import { RecipeService } from '../../core/services/recipe.service';

@Injectable({
  providedIn: 'root'
})
export class RecipeResolveService implements Resolve<{recipes_list: Recipe[]}> {
  constructor(
    private readonly recipeService: RecipeService,
  ) {}

  public resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.recipeService.getAllRecipes().pipe(
      catchError(res => {
        // console.log('there is a problem in recipe resolver');
        return of({recipes_list: []});
      })
    );
  }
}
