import {
  Component,
  ComponentFactory,
  ComponentFactoryResolver,
  ComponentRef,
  OnDestroy,
  OnInit,
  ViewChild,
  ViewContainerRef,
  Type,
  NgZone,
  AfterViewInit
} from '@angular/core';
import {
  ActivatedRoute
} from '@angular/router';
import {
  DndDropEvent,
  DropEffect
} from 'ngx-drag-drop';
import {
  field,
  value
} from 'src/global.model';
import Swal from 'sweetalert2';
import {
  IngredientComponent
} from '../../ingredient/ingredient.component';
import {
  RecipeService
} from 'src/app/core/services/recipe.service';
import {
  Ingredient
} from 'src/app/common/interfaces/ingredient';
import {
  SubrecipeComponent
} from '../../subrecipe/subrecipe.component';
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import {
  NutritionCalculationService
} from 'src/app/core/services/nutrition-calculation.service';
import {
  Recipe
} from 'src/app/common/interfaces/recipe';
import {
  NutritionFull
} from 'src/app/common/interfaces/nutrition-full';
@Component({
  selector: 'app-recipe-builder',
  templateUrl: './recipe-builder.component.html',
  styleUrls: ['./recipe-builder.component.css']
})
export class RecipeBuilderComponent implements OnInit, OnDestroy {
  value: value = {
    label: "",
    value: ""
  }
  recipeDetails: any;
  success = false;
  selected: string;
  totalWeight: number
  index: number = 0;
  subrecipeIndex: number = 0;
  private chart: am4charts.PieChart;
  nutritionSummary
  ingredientViews
  subrecipes
  recipeDescription: string
  courseOption
  recipeIngredients
  ingrediantsToReturn

  @ViewChild('ing', {
    read: ViewContainerRef,
    static: false
  }) container: ViewContainerRef

  @ViewChild('subrecipe', {
    read: ViewContainerRef,
    static: false
  }) subrecipeContainer: ViewContainerRef

  subrecipeComponentRef: ComponentRef < SubrecipeComponent >
  componentRef: ComponentRef < IngredientComponent >
  nutritionFull: NutritionFull;
  fooIngredientComponent = IngredientComponent
  fooSubrecipeComponent = SubrecipeComponent
  
  subrecipeComponents = []
  components = []

  createSubrecipeComponent() {
    const subrecipefactory: ComponentFactory < any > = this.resolver.resolveComponentFactory(SubrecipeComponent);
    this.subrecipeComponentRef = this.subrecipeContainer.createComponent(subrecipefactory);
    this.subrecipeComponents.push(this.subrecipeComponentRef);

    let currentSubrecipeComponent = this.subrecipeComponentRef.instance;

    currentSubrecipeComponent.selfRef1 = currentSubrecipeComponent;
    currentSubrecipeComponent.compInteraction = this
    currentSubrecipeComponent.subrecipeIndex = ++this.subrecipeIndex;
  }

  removeSubrecipeComponent(subrecipeIndex1) {

    if (this.subrecipeContainer.length < 1)
      return;


    let SubrecipeComponentRef = this.subrecipeComponents.filter(x => x.instance.subrecipeIndex == subrecipeIndex1)[0];


    let subcomponent: SubrecipeComponent = < SubrecipeComponent > SubrecipeComponentRef.instance;

    let vcrIndex1: number = this.subrecipeContainer.indexOf(SubrecipeComponentRef)

    this.subrecipeContainer.remove(vcrIndex1);

    this.subrecipeComponents = this.subrecipeComponents.filter(x => x.instance.subrecipeIndex !== this.subrecipeIndex);
  }

  createComponent() {
    const factory: ComponentFactory < any > = this.resolver.resolveComponentFactory(IngredientComponent);
    this.componentRef = this.container.createComponent(factory);
    this.components.push(this.componentRef);

    let currentComponent = this.componentRef.instance;

    currentComponent.selfRef = currentComponent;
    currentComponent.index = ++this.index;
    currentComponent.compInteraction = this
  }

  removeComponent(index) {

    if (this.container.length < 1)
      return;
    
    let componentRef = this.components.filter(x => x.instance.index == index)[0];
    let component: IngredientComponent = < IngredientComponent > componentRef.instance;

    let vcrIndex: number = this.container.indexOf(componentRef)

    this.container.remove(vcrIndex);

    this.components = this.components.filter(x => x.instance.index !== index);
    if (this.container.length < 1){
      this.removeField('text')
    }
    
  }
  
  fieldModels: Array < field >= [{
      "type": "text",
      "icon": "fa fa-pie-chart",
      "label": "Ingredient",
      "description": "Enter your Ingredients",
      "placeholder": "Enter your Ingredients",
      "className": Math.random(),
      "subtype": "text",
      "regex": "",
      "handle": true,
    },
    {
      "type": "email",
      "icon": "fa fa-list-alt",
      "label": "Recipe",
    },
    {
      "type": "textarea",
      "icon": "fa-text-width",
      "label": "Description",
      "description": "not",
    },
    {
      "type": "radio",
      "icon": "fa-list-ul",
      "label": "Courses",
      "description": "Radio boxes",
      "values": [{
          "label": "No category",
          "value": "0"
        },
        {
          "label": "Pasta",
          "value": "1"
        },
        {
          "label": "Risotto",
          "value": "2"
        },
        {
          "label": "Salads",
          "value": "3"
        },
        {
          "label": "Breads,Muffin,Scones",
          "value": "4"
        },
        {
          "label": "Soups",
          "value": "5"
        },
        {
          "label": "BBQ",
          "value": "6"
        },
        {
          "label": "Pizzas",
          "value": "7"
        },
        {
          "label": "Sandwiches,Wraps",
          "value": "8"
        },
        {
          "label": "Pastries and cakes",
          "value": "9"
        },
        {
          "label": "Desserts",
          "value": "10"
        },
        {
          "label": "Sauces",
          "value": "11"
        },
        {
          "label": "Drinks",
          "value": "12"
        }
      ]
    },
    {
      "type": "file",
      "icon": "fa-file",
      "label": "Upload Image",
      "className": "form-control",
      "subtype": "file",
      "description": "not",
    }
  ];

  modelFields: Array < field >= [];
  model: any = {
    name: 'Recipe Name',
    description: 'Recipe Description...',
    theme: {
      bgColor: "ffffff",
      textColor: "555555",
      bannerImage: ""
    },
    attributes: this.modelFields
  };

  report = false;
  reports: any = [];

  constructor(
    private route: ActivatedRoute,
    private resolver: ComponentFactoryResolver,
    private recipeService: RecipeService,
    private zone: NgZone,
    private readonly nutriCalc: NutritionCalculationService
  ) {}
  

  ngOnDestroy() {
    this.zone.runOutsideAngular(() => {
      if (this.chart) {
        this.chart.dispose();
      }
    });
  }
  ngOnInit() {
  }

  onDragStart(event: DragEvent) {
    console.log("drag started", JSON.stringify(event, null, 2));
  }

  onDragEnd(event: DragEvent) {
    console.log("drag ended", JSON.stringify(event, null, 2));
  }

  onDraggableCopied(event: DragEvent) {
    console.log("draggable copied", JSON.stringify(event, null, 2));
  }

  onDraggableLinked(event: DragEvent) {
    console.log("draggable linked", JSON.stringify(event, null, 2));
  }

  onDragged(item: any, list: any[], effect: DropEffect) {
    if (effect === "move") {
      const index = list.indexOf(item);
      list.splice(index, 1);
    }
  }

  onDragCanceled(event: DragEvent) {
    console.log("drag cancelled", JSON.stringify(event, null, 2));
  }

  onDragover(event: DragEvent) {
    console.log("dragover", JSON.stringify(event, null, 2));
  }

  onDrop(event: DndDropEvent, list ? : any[]) {
   
  
    if(event.data.label="Ingrediant" ){
      this.addFirstField() }
    if (list && (event.dropEffect === "copy" || event.dropEffect === "move")) {

      if (event.dropEffect === "copy")
        event.data.name = event.data.type + '-' + new Date().getTime();
      let index = event.index;
      if (typeof index === "undefined") {
        index = list.length;
      }
      list.splice(index, 0, event.data);
    }
    
  }

  addValue(values) {
    values.push(this.value);
    this.value = {
      label: "",
      value: ""
    };
  }

  removeField(i) {
    Swal.fire({
      title: 'Are you sure?',
      text: "Do you want to remove this field?",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#00B96F',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, remove!'
    }).then((result) => {
      if (result.value) {
        this.model.attributes.splice(i, 1);
      }
    });

  }


  initReport() {
    this.report = true;
    let input = {
      id: this.model._id
    }
  }


  receipeName: string
  toggleValue(item) {
    item.selected = !item.selected;
  }

  

  submit() {
    if (!this.receipeName) {
      Swal.fire('Error', 'Please enter Recipe Name');
      return
    }
    this.ingredientViews = this.components
    this.ingrediantsToReturn = this.ingredientViews.map(x => {
      let measure = x.instance.selectedMeasure
      
      const measure2=x.instance.listOfMeasurments.map(x=>{return x.measure})
      
      if (measure===x.instance.selected) {
        measure = 'g'
      }
      
      let obj: Ingredient = {
        productName: x.instance.selected,
        amount: x.instance.incrementedValue,
        selectedMeasure: measure
      }
      return obj
    })
    
    this.subrecipes = this.subrecipeComponents.map(x => {
      let subrecipeObj = {
        subrecipeName: x.instance.receipeToReturn,
        amount: x.instance.incrementedValue,
        overallNutrition:(x.instance.overallNutrition/100)*x.instance.incrementedValue
      }
      return subrecipeObj
    })
    this.recipeIngredients = this.ingrediantsToReturn
    let recipeCategory = this.courseOption
    if (recipeCategory === undefined) {
      recipeCategory = "0"
    }
    let recipeSubreceryes = this.subrecipes
    if (!recipeSubreceryes) {
      recipeSubreceryes = []
    }

    const allNutrition = this.components.map(x => {
      const nutrientCal = (x.instance.nutrientCal/100)*x.instance.amount
      return nutrientCal
    }).reduce((a, b) => +a + +b)
    let subrecipesOverallNutrition=[]
    if(this.subrecipes[0] != undefined){
      subrecipesOverallNutrition=this.subrecipes.map(x=>{return +x.overallNutrition}).reduce((a, b) => +a + +b)
    }
    
    const createRecipe = {
      title: this.receipeName,
      ingredients: this.recipeIngredients,
      usedSubrecipesIds: recipeSubreceryes,
      category: +recipeCategory,
      information: this.recipeDescription,
      overallNutrition:(+allNutrition + +subrecipesOverallNutrition)
    }
    Swal.fire({
      position: 'top-end',
      type: 'success',
      title: 'Your Recipe has been saved',
      showConfirmButton: false,
      timer: 1500
    })
    this.recipeService.createRecipe(createRecipe).subscribe((params) => {
    }, (err: any) => {
      
      Swal.fire({
        type: 'error',
        title: 'Oops...',
        text: 'Duplicate Recipe Name OR Improper ingredient'

      })
    })
  }


  recipeNutrition: {
    nutrient: string,
    value: number
  } [];

  addFirstField(){
  let check=this.model.attributes.filter(x=>{return x.label='Ingrediant'})
  
  if(check.length<1){
  this.createComponent()}
}


  analyseRecipe() {
    
    let cal = this.components.map(x => {
      let instance = Object.values(x.instance.itemNutrients)
      const amount = x.instance.amount
      let newinstance = instance.map((x: any) => {
        return x.value / 100 * amount
      })
      return newinstance
    })
    let cal2 = cal.reduce(function (array1, array2) {
      return array1.map(function (value, index) {
        return value + array2[index];
      });
    });

    this.recipeDetails = {
      ingredients: {
        product: {
          ingredients: this.recipeIngredients
        }
      }
    }

    const ingrediantsToAnalyseCal = this.components.map(x => {
      const nutrientCal = x.instance.nutrientCal
      return nutrientCal
    }).reduce((a, b) => +a + +b)
    const ingrediantsToAnalysePro = this.components.map(x => {
      const nutrientProtein = x.instance.nutrientProtein
      return nutrientProtein
    }).reduce((a, b) => +a + +b)
    const ingrediantsToAnalyseCarbs = this.components.map(x => {
      const nutrientCarbs = x.instance.nutrientCarbs
      return nutrientCarbs
    }).reduce((a, b) => +a + +b)
    const ingrediantsToAnalyseFat = this.components.map(x => {
      const nutrientFat = x.instance.nutrientFat
      return nutrientFat
    }).reduce((a, b) => +a + +b)

    this.recipeNutrition = [{
        nutrient: "kcal",
        value: +ingrediantsToAnalyseCal
      },
      {
        nutrient: "carbs",
        value: +ingrediantsToAnalyseCarbs
      },
      {
        nutrient: "fat",
        value: +ingrediantsToAnalyseFat
      },
      {
        nutrient: "proteins",
        value: +ingrediantsToAnalysePro
      },
    ]


    /* CHART-1 */
    this.zone.runOutsideAngular(() => {
      let chart = am4core.create("chart-1", am4charts.PieChart);

      chart.hiddenState.properties.opacity = 0; // this creates initial fade-in

      chart.paddingTop = 0;
      let data = this.nutritionSummary;
      chart.data = [{
          "nutrient": "carbs",
          "value": +ingrediantsToAnalyseCarbs
        },
        {
          "nutrient": "fat",
          "value": +ingrediantsToAnalyseFat
        },
        {
          "nutrient": "proteins",
          "value": +ingrediantsToAnalysePro
        }
      ];

      //  Add label
      chart.innerRadius = am4core.percent(50);
      let label = chart.seriesContainer.createChild(am4core.Label);
      label.text = "per 100g";
      label.horizontalCenter = "middle";
      label.verticalCenter = "middle";
      label.fontSize = 13;

      // Add and configure Series
      let pieSeries = chart.series.push(new am4charts.PieSeries());
      pieSeries.colors.list = [
        am4core.color("#628800"),
        am4core.color("#aed407"),
        am4core.color("#C8EE21"),
      ];
      pieSeries.ticks.template.disabled = true;
      pieSeries.labels.template.disabled = true;
      pieSeries.dataFields.category = "nutrient";
      pieSeries.dataFields.value = "value";

      pieSeries.hiddenState.properties.startAngle = 90;
      pieSeries.hiddenState.properties.endAngle = 90;

      this.chart = chart;
    });

    this.zone.runOutsideAngular(() => {
      let chart = am4core.create("chart-2", am4charts.PieChart);
      chart.hiddenState.properties.opacity = 0; 
      chart.paddingTop = 0;
      chart.data = [{
          "nutrient": "sugar",
          "value": cal2[4]
        },
        {
          "nutrient": "fiber",
          "value": cal2[5]
        },
        {
          "nutrient": "calcium",
          "value": cal2[6]
        },
        {
          "nutrient": "iron",
          "value": cal2[7]
        },
        {
          "nutrient": "phosphorus",
          "value": cal2[8]
        },
        {
          "nutrient": "potassium",
          "value": cal2[9]
        },
        {
          "nutrient": "sodium",
          "value": cal2[10]
        },
        {
          "nutrient": "vitaminA",
          "value": cal2[11]
        },
        {
          "nutrient": "vitaminE",
          "value": cal2[12]
        },
        {
          "nutrient": "vitaminD",
          "value": cal2[13]
        },
        {
          "nutrient": "vitaminC",
          "value": cal2[14]
        },
        {
          "nutrient": "vitaminB12",
          "value": cal2[15]
        },
        {
          "nutrient": "folicAcid",
          "value": cal2[16]
        },
        {
          "nutrient": "cholesterol",
          "value": cal2[17]
        },
      ];
      chart.innerRadius = am4core.percent(50);
      let label = chart.seriesContainer.createChild(am4core.Label);
      label.text = "per 100g";
      label.horizontalCenter = "middle";
      label.verticalCenter = "middle";
      label.fontSize = 13;

      let pieSeries = chart.series.push(new am4charts.PieSeries());
      pieSeries.colors.list = [
        am4core.color("#628800"),
        am4core.color("#aed407"),
        am4core.color("#C8EE21"),
        am4core.color("#628800"),
        am4core.color("#aed407"),
        am4core.color("#C8EE21"),
        am4core.color("#628800"),
        am4core.color("#aed407"),
        am4core.color("#C8EE21"),
        am4core.color("#628800"),
        am4core.color("#aed407"),
        am4core.color("#C8EE21"),
        am4core.color("#628800"),
        am4core.color("#aed407"),
      ];
      pieSeries.ticks.template.disabled = true;
      pieSeries.labels.template.disabled = true;
      pieSeries.dataFields.value = "value";
      pieSeries.dataFields.category = "nutrient";

      pieSeries.hiddenState.properties.startAngle = 90;
      pieSeries.hiddenState.properties.endAngle = 90;

      this.chart = chart;
    });
  }



}
