import { NgModule } from '@angular/core';
import { RecipeViewerComponent } from './recipe-viewer/recipe-viewer.component';
import { RecipeRoutingModule } from './recipe-routing.module';
import { RecipeBuilderComponent } from './recipe-builder/recipe-builder.component';
import { RecipeResolveService } from './recipe-resolver.service';
import { SharedModule } from 'src/app/shared/shared.module';
import { SingleRecipeDetailsComponent } from './single-recipe-details/single-recipe-details.component';
import { SingleRecipeCardComponent } from './recipe-viewer/single-recipe-card/single-recipe-card.component';
import { RecipeEditComponent } from './recipe-edit/recipe-edit.component';
import { RecipeAnalyzerComponent } from './recipe-analyzer/recipe-analyzer.component';
import { DetailsRecipeResolverService } from './details-recipe-resolver.service';
import { SingleIngredientComponent } from './single-recipe-details/single-ingredient/single-ingredient.component';

@NgModule({
  declarations: [
    RecipeViewerComponent,
    RecipeBuilderComponent,
    SingleRecipeDetailsComponent,
    SingleRecipeCardComponent,
    RecipeEditComponent,
    RecipeAnalyzerComponent,
    SingleIngredientComponent,
  ],
  imports: [
    SharedModule,
    RecipeRoutingModule,
  ],
  providers: [
    RecipeResolveService,
    DetailsRecipeResolverService,
  ],
})
export class RecipeModule { }
