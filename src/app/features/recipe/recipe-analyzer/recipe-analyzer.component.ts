import { Component, OnInit } from '@angular/core';
import { Ingredient } from 'src/app/common/interfaces/ingredient';
import { Router, NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-recipe-analyzer',
  templateUrl: './recipe-analyzer.component.html',
  styleUrls: ['./recipe-analyzer.component.css']
})
export class RecipeAnalyzerComponent implements OnInit {
  recipeDescription
  constructor(private readonly router: Router) { }

  ngOnInit() {
    
  }
  submit(){
    let splited=this.recipeDescription.split(/\r\n|\n|\r/)
    let mapIngrediants=splited.map(x=>{
      let splitedArr=x.split(' ')

      let obj:Ingredient={
        productName:splitedArr[2].trim(),
        amount:splitedArr[0].trim(),
        selectedMeasure:splitedArr[1].trim()}
      return obj

    })

  }
  
  toEdit(event) {
    let splited=this.recipeDescription.split(/\r\n|\n|\r/)
    let mapIngrediants=splited.map(x=>{
      let splitedArr=x.split('`')

      let obj:Ingredient={
        productName:splitedArr[2].trim(),
        amount:splitedArr[0].trim(),
        selectedMeasure:splitedArr[1].trim()}
      return obj

    })


    let navigationExtras: NavigationExtras = {
      queryParams: {add:JSON.stringify(mapIngrediants)}
  };
    const newEvent=JSON.stringify(this.recipeDescription)
    this.router.navigate([`/recipes/edit/1`],navigationExtras);
    
  }
}
