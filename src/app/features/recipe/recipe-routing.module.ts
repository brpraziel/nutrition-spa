import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RecipeViewerComponent } from './recipe-viewer/recipe-viewer.component';
import { RecipeBuilderComponent } from './recipe-builder/recipe-builder.component';
import { RecipeResolveService } from './recipe-resolver.service';
import { SingleRecipeDetailsComponent } from './single-recipe-details/single-recipe-details.component';
import { DetailsRecipeResolverService } from './details-recipe-resolver.service';
import { RecipeEditComponent } from './recipe-edit/recipe-edit.component';
import { RecipeAnalyzerComponent } from './recipe-analyzer/recipe-analyzer.component';


const routes: Routes = [
  {
    path: '',
    component: RecipeViewerComponent,
    resolve: {
      recipes_list: RecipeResolveService,
    },
  },
  {
    path: 'all-recipes',
    component: RecipeViewerComponent,
    resolve: {
      recipes_list: RecipeResolveService,
    },
  },
  {
    path: 'favorites',
    component: RecipeViewerComponent,
  },
  {
    path: 'new-recipe',
    component: RecipeBuilderComponent,
    resolve: {
      recipes_list: RecipeResolveService,
    },
  },
  {
    path: 'analyzer',
    component: RecipeAnalyzerComponent,
  },
  {
    path: 'details/:title',
    component: SingleRecipeDetailsComponent,
    resolve: {
      recipe_details: DetailsRecipeResolverService,
    },
  },
  {
    path: 'edit/:title',
    component: RecipeEditComponent,
    resolve: {
      recipe_details: DetailsRecipeResolverService,
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RecipeRoutingModule {}
