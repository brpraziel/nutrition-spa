// import { Injectable } from '@angular/core';
// import {
//   ActivatedRouteSnapshot,
//   Resolve,
//   RouterStateSnapshot
// } from '@angular/router';
// import { of } from 'rxjs';
// import { catchError } from 'rxjs/operators';
// import { RecipeService } from '../../core/services/recipe.service';
// import { Recipe } from '../../common/interfaces/recipe';
// import { SubrecipeDescriptor } from 'src/app/common/interfaces/subrecipe-descriptor';

// @Injectable({
//   providedIn: 'root'
// })
// export class SubrecipeResolverService implements Resolve<{ subrecipes: Recipe[] }> {
//   recipeTitle: string = '';
//   subrecipesArray: Recipe[] = [];

//   constructor(
//     private readonly recipeService: RecipeService,
//   ) {}

//   public resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
//     this.recipeTitle = route.paramMap.get('title');

//     this.recipeService.getSingleRecipeByTitle(this.recipeTitle).subscribe(
//         parentRecipeResolved => {
//             const usedSubrecipesArr: any[] = parentRecipeResolved.recipe_details[0].usedSubrecipesIds;

//             usedSubrecipesArr.map( (recipe: SubrecipeDescriptor) => {
//                 this.recipeService.getRecipeByTitle(recipe.subrecipeName).subscribe(
//                   successData => {
//                     this.subrecipesArray.push(successData.subrecipes[0]);
//                   },
//                   error => {
//                     console.log('Error in subrecipe resolver (children resolve case).');
//                   }
//                 );
//             });
//         },
//         error => {
//             console.log('Error in subrecipe resolver (parent resolve case).');
//         }
//     );

//     while (this.subrecipesArray.length <= 0) {
//         return of({ subrecipes: []});
//     }

//     return of({ subrecipes: this.subrecipesArray });
//   }
// }
