import { Component, OnInit, Input } from '@angular/core';
import { Recipe } from '../../../common/interfaces/recipe';
import { ActivatedRoute } from '@angular/router';
import { NutritionCalculationService } from '../../../core/services/nutrition-calculation.service';

import { NutritionFull } from '../../../common/interfaces/nutrition-full';
import { Ingredient } from 'src/app/common/interfaces/ingredient';
import { PrintService } from 'src/app/core/services/print.service';
import { RecipeService } from 'src/app/core/services/recipe.service';


@Component({
  selector: 'app-recipe-print',
  templateUrl: './recipe-print.component.html',
  styleUrls: ['./recipe-print.component.css']
})
export class RecipePrintComponent implements OnInit {
  nutritionFull: NutritionFull;

  recipeIngredients: Ingredient[];
  recipeNutrition: {nutrient: string, value: number}[];

  @Input()
  recipeDetails: Recipe;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly nutriCalc: NutritionCalculationService,
    private readonly recipeService: RecipeService,
    private readonly printService: PrintService,
    ) { }

    ngOnInit() {
      this.route.data.subscribe(
        data => {
          if (data.recipe_details.length > 0) {
            this.recipeDetails = data.recipe_details[0];
            this.recipeIngredients = data.recipe_details[0].ingredients;
            setTimeout(window.print);
          } else {
            return console.log('Error in retrieval of recipe details.');
          }
        },
        error => {
          console.log("An error ocurred while taking data from recipe's resolvers.");
        }
      );

      const data = this.nutriCalc.calculateFullNutritionForRecipe(this.recipeDetails);

      /* formatting data for table view */
      this.recipeNutrition = [
        {nutrient: "kcal", value: data.energy},
        {nutrient: "carbs", value: data.carbs},
        {nutrient: "fat", value: data.fat},
        {nutrient: "proteins", value: data.proteins},
        {nutrient: "sugar", value: data.sugar},
        {nutrient: "fiber", value: data.fiber},
        {nutrient: "calcium", value: data.calcium},
        {nutrient: "iron", value: data.iron},
        {nutrient: "phosphorus", value: data.phosphorus},
        {nutrient: "potassium", value: data.potassium},
        {nutrient: "sodium", value: data.sodium},
        {nutrient: "vitamin A", value: data.vitaminA},
        {nutrient: "vitamin E", value: data.vitaminE},
        {nutrient: "vitamin D", value: data.vitaminD},
        {nutrient: "vitamin C", value: data.vitaminC},
        {nutrient: "vitamin B12", value: data.vitaminB12},
        {nutrient: "folic acid", value: data.folicAcid},
        {nutrient: "cholesterol", value: data.cholesterol},
      ];
  }

}
