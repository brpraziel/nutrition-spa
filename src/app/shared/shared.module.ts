import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { DndModule } from 'ngx-drag-drop';
import { IngredientComponent } from '../features/ingredient/ingredient.component';
import { ItemsComponent } from '../components/items/items.component';
import { FullCalendarModule } from '@fullcalendar/angular';
import { PrintLayoutComponent } from '../features/print/print-layout/print-layout.component';
import { RecipePrintComponent } from '../features/recipe/recipe-print/recipe-print.component';
import { NgxPaginationModule } from 'ngx-pagination';

@NgModule({
  providers: [],
  declarations: [
    IngredientComponent,
    ItemsComponent,
    PrintLayoutComponent,
    RecipePrintComponent,
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    RouterModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    DndModule,
    FullCalendarModule,
    NgxPaginationModule,
  ],
  exports: [
    CommonModule,
    RouterModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    DndModule,
    IngredientComponent,
    ItemsComponent,
    FullCalendarModule,
    PrintLayoutComponent,
    RecipePrintComponent,
    NgxPaginationModule,
  ]
})
export class SharedModule { }
