import { Component, OnInit } from '@angular/core';
import { AuthService } from './core/services/auth.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'nutrition';
  // isLoggedIn: boolean = false;

  subscription: Subscription;

  constructor(
    private readonly authService: AuthService,
  ) {}

  ngOnInit() {
    this.subscription = this.authService.user$.subscribe(
      success => {
        // if (success.length > 0) {
        //   this.isLoggedIn = true;
        // }
      },
      error => {
        console.log('Sorry, something went wrong with user login.');
      }
    );
  }

}
