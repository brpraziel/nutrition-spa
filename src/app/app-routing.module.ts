import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { AuthGuardService as AuthGuard } from '../app/auth/guards/auth-guard.service';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { PrintLayoutComponent } from './features/print/print-layout/print-layout.component';
import { RecipePrintComponent } from './features/recipe/recipe-print/recipe-print.component';
import { PrintRecipeResolverService } from './features/recipe/print-recipe-resolver.service';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'recipes',
    loadChildren: './features/recipe/recipe.module#RecipeModule',
   // canActivate: [AuthGuard],
  },
  { path: 'calendar',
    loadChildren: './features/calendar/calendar.module#AppCalendarModule',
  },
  { path: 'print',
    outlet: 'print',
    component: PrintLayoutComponent,
    children: [
      {
        path: 'printer/:title',
        component: RecipePrintComponent,
        resolve: {
          recipe_details: PrintRecipeResolverService,
        },
      },
    ]
  },
  { path: 'not-found', component: NotFoundComponent },
  { path: '**', redirectTo: '/not-found' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules, onSameUrlNavigation: 'reload' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
