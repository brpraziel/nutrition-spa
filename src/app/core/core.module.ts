import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotificatorService } from './services/notificator.service';
import { ToastrModule } from 'ngx-toastr';
import { StorageService } from './services/storage.service';
import { AuthService } from './services/auth.service';
import { NutritionCalculationService } from './services/nutrition-calculation.service';
import { PrintService } from './services/print.service';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ToastrModule.forRoot(),
  ],
  providers: [
    StorageService,
    NotificatorService,
    AuthService,
    NutritionCalculationService,
    PrintService,
  ],
})
export class CoreModule { }
