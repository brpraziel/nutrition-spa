import { Injectable } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestoreDocument, AngularFirestore } from 'angularfire2/firestore';
import { Item } from '../../common/models/item.dto';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class ItemService {
itemscolection:AngularFirestoreCollection<Item>
items: Observable<Item[]>
  constructor(private afs: AngularFirestore) { 
    this.items=this.afs.collection('products',ref=>{return ref.where('NDB_No','==',1001)}).valueChanges()
  }
  getItems(){
    return this.items
  }
}
