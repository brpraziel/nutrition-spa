import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NavbarService {
  private isVisible = false;

  readonly toggleEvent$ = new BehaviorSubject<boolean>(this.isVisible);

  constructor() {}

  public triggerToggleEvent(): void {
    this.isVisible = !this.isVisible;
    this.toggleEvent$.next(this.isVisible);
  }
}
