import { TestBed } from '@angular/core/testing';
import { HttpClient } from '@angular/common/http';
import { RecipeService } from './recipe.service';
import { of } from 'rxjs';


describe('RecipeService', () => {
    const http = jasmine.createSpyObj('HttpClient', ['get', 'post', 'put', 'delete']);
    beforeEach( () => TestBed.configureTestingModule({
        providers: [
            {
                provide: HttpClient,
                useValue: http,
            },
        ]
    }));

    it('RecipeService should be initialized', () => {
        // Arrange
       const service: RecipeService = TestBed.get(RecipeService);

       // Act & Assert
       expect(service).toBeTruthy();
    });

    it('getAllRecipes() should return an object with key \'recipes_list\' that holds as a value an array of recipes', () => {
        // Arrange
        const eggProduct = {
            description: 'egg',
        };

        const eggIngredient = {
            amount: 3,
            product: eggProduct,
            selectedMeasure: 'medium',
        };

        http.get.and.returnValue(of({
            recipes_list: [
                {
                    title: 'Pancakes',
                    ingredients: [eggIngredient],
                    overallNutrition: 250,
                    information: 'Easy recipe',
                }
            ]
        }));

        // Act & Assert
        const service: RecipeService = TestBed.get(RecipeService);
        service.getAllRecipes().subscribe(
            response => {
                expect(response).toEqual(
                    {
                        recipes_list: [
                            {
                                title: 'Pancakes',
                                ingredients: [eggIngredient],
                                overallNutrition: 250,
                                information: 'Easy recipe',
                            }
                        ]
                    }
                );
            }
        );
    });

    it('getAllRecipes() should call http.get only once', () => {
        // Arrange
        const service: RecipeService = TestBed.get(RecipeService);

        const eggProduct = {
            description: 'egg',
        };

        const eggIngredient = {
            amount: 3,
            product: eggProduct,
            selectedMeasure: 'medium',
        };

        http.get.and.returnValue(of({
            recipes_list: [
                {
                    title: 'Pancakes',
                    ingredients: [eggIngredient],
                    overallNutrition: 250,
                    information: 'Easy recipe',
                }
            ]
        }));


        // Act & Assert
        http.get.calls.reset(); // to ensure other http calls don't get involved in the number of calls
        service.getAllRecipes().subscribe(
            () => expect(http.get).toHaveBeenCalledTimes(1)
        );
    });

    // tslint:disable-next-line: max-line-length
    it('getSingleRecipeByTitle(recipeTitle: string) should return an object with key \'recipe_details\' that holds as a value an array of recipes', () => {
        // Arrange
        const eggProduct = {
            description: 'egg',
        };

        const eggIngredient = {
            amount: 3,
            product: eggProduct,
            selectedMeasure: 'medium',
        };

        const recipeTitle = 'Pancakes';

        http.get.and.returnValue(of({
            recipe_details: [
                {
                    title: 'Pancakes',
                    ingredients: [eggIngredient],
                    overallNutrition: 250,
                    information: 'Easy recipe',
                }
            ]
        }));

        // Act & Assert
        const service: RecipeService = TestBed.get(RecipeService);
        service.getSingleRecipeByTitle(recipeTitle).subscribe(
            response => {
                expect(response).toEqual(
                    {
                        recipe_details: [
                            {
                                title: 'Pancakes',
                                ingredients: [eggIngredient],
                                overallNutrition: 250,
                                information: 'Easy recipe',
                            }
                        ]
                    }
                );
            }
        );
    });

    it('getSingleRecipeByTitle(recipeTitle: string) should call http.get only once', () => {
        // Arrange
        const service: RecipeService = TestBed.get(RecipeService);

        const eggProduct = {
            description: 'egg',
        };

        const eggIngredient = {
            amount: 3,
            product: eggProduct,
            selectedMeasure: 'medium',
        };

        const recipeTitle = 'Pancakes';

        http.get.and.returnValue(of({
            recipe_details: [
                {
                    title: 'Pancakes',
                    ingredients: [eggIngredient],
                    overallNutrition: 250,
                    information: 'Easy recipe',
                }
            ]
        }));

        // Act & Assert
        http.get.calls.reset(); // to ensure other http calls don't get involved in the number of calls
        service.getSingleRecipeByTitle(recipeTitle).subscribe(
            () => expect(http.get).toHaveBeenCalledTimes(1)
        );
    });

    it('createRecipe(recipeBody: Recipe) should return the created recipe', () => {
        // Arrange
        const eggProduct = {
            description: 'egg',
        };

        const eggIngredient = {
            amount: 3,
            product: eggProduct,
            selectedMeasure: 'medium',
        };

        const recipeBody = {
            title: 'Pancakes',
            ingredients: [eggIngredient],
            overallNutrition: 250,
            information: 'Easy recipe',
        };

        http.post.and.returnValue(of(
            {
                title: 'Pancakes',
                ingredients: [eggIngredient],
                overallNutrition: 250,
                information: 'Easy recipe',
            }
        ));

        // Act & Assert
        const service: RecipeService = TestBed.get(RecipeService);
        service.createRecipe(recipeBody).subscribe(
            response => {
                expect(response).toEqual(
                    {
                        title: 'Pancakes',
                        ingredients: [eggIngredient],
                        overallNutrition: 250,
                        information: 'Easy recipe',
                    }
                );
            }
        );
    });

    it('createRecipe(recipeBody: Recipe) should call http.post only once', () => {
        // Arrange
        const eggProduct = {
            description: 'egg',
        };

        const eggIngredient = {
            amount: 3,
            product: eggProduct,
            selectedMeasure: 'medium',
        };

        const recipeBody = {
            title: 'Pancakes',
            ingredients: [eggIngredient],
            overallNutrition: 250,
            information: 'Easy recipe',
        };

        http.post.and.returnValue(of(
            {
                title: 'Pancakes',
                ingredients: [eggIngredient],
                overallNutrition: 250,
                information: 'Easy recipe',
            }
        ));

        // Act & Assert
        http.post.calls.reset(); // to ensure other http calls don't get involved in the number of calls
        const service: RecipeService = TestBed.get(RecipeService);
        service.createRecipe(recipeBody).subscribe(
            () => expect(http.post).toHaveBeenCalledTimes(1)
        );
    });

    it('archiveRecipeById(recipeId: string) should return a message', () => {
        // Arrange
        const recipeId = 'efc9857b-af4f-47e5-b02b-d2f6e3f2a6ef';

        http.delete.and.returnValue(of(
            {
                message: 'This recipe was deleted!'
            }
        ));

        // Act & Assert
        const service: RecipeService = TestBed.get(RecipeService);
        service.archiveRecipeById(recipeId).subscribe(
            response => {
                expect(response).toEqual(
                    {
                        message: 'This recipe was deleted!'
                    }
                );
            }
        );
    });

    it('archiveRecipeById(recipeId: string) should call http.delete only once', () => {
        // Arrange
        const service: RecipeService = TestBed.get(RecipeService);

        const recipeId = 'efc9857b-af4f-47e5-b02b-d2f6e3f2a6ef';

        http.delete.and.returnValue(of(
            {
                message: 'This recipe was deleted!'
            }
        ));


        // Act & Assert
        http.delete.calls.reset(); // to ensure other http calls don't get involved in the number of calls
        service.archiveRecipeById(recipeId).subscribe(
            () => expect(http.delete).toHaveBeenCalledTimes(1)
        );
    });

    it('editRecipe(id: string, recipeBody: Recipe) should return the created recipe', () => {
        // Arrange
        const eggProduct = {
            description: 'egg',
        };

        const eggIngredient = {
            amount: 3,
            product: eggProduct,
            selectedMeasure: 'medium',
        };

        const recipeId = 'efc9857b-af4f-47e5-b02b-d2f6e3f2a6ef';

        const recipeBody = {
            id: recipeId,
            title: 'Pancakes',
            ingredients: [eggIngredient],
            overallNutrition: 250,
            information: 'Easy recipe',
        };

        http.put.and.returnValue(of(
            {
                id: recipeId,
                title: 'Pancakes',
                ingredients: [eggIngredient],
                overallNutrition: 250,
                information: 'Easy recipe',
            }
        ));

        // Act & Assert
        const service: RecipeService = TestBed.get(RecipeService);
        service.editRecipe(recipeId, recipeBody).subscribe(
            response => {
                expect(response).toEqual(
                    {
                        id: recipeId,
                        title: 'Pancakes',
                        ingredients: [eggIngredient],
                        overallNutrition: 250,
                        information: 'Easy recipe',
                    }
                );
            }
        );
    });

    it('editRecipe(id: string, recipeBody: Recipe) should call http.put only once', () => {
        // Arrange
        const service: RecipeService = TestBed.get(RecipeService);

        const eggProduct = {
            description: 'egg',
        };

        const eggIngredient = {
            amount: 3,
            product: eggProduct,
            selectedMeasure: 'medium',
        };

        const recipeId = 'efc9857b-af4f-47e5-b02b-d2f6e3f2a6ef';

        const recipeBody = {
            id: recipeId,
            title: 'Pancakes',
            ingredients: [eggIngredient],
            overallNutrition: 250,
            information: 'Easy recipe',
        };

        http.put.and.returnValue(of(
            {
                id: recipeId,
                title: 'Pancakes',
                ingredients: [eggIngredient],
                overallNutrition: 250,
                information: 'Easy recipe',
            }
        ));

        // Act & Assert
        http.put.calls.reset(); // to ensure other http calls don't get involved in the number of calls
        service.editRecipe(recipeId, recipeBody).subscribe(
            () => expect(http.put).toHaveBeenCalledTimes(1)
        );
    });

});


