import { TestBed } from '@angular/core/testing';
import { CalculationService } from './calculation.service';



describe('CalculationService', () => {
    const calculationService=jasmine.createSpyObj('CalculationService',['gramsCalculation','nutrientCalculation'])
    beforeEach(() => TestBed.configureTestingModule({
        providers: [{
            provide: CalculationService,
                useValue: calculationService,
        }
        ]
    }));

    it('should be created', () => {
        const service: CalculationService = TestBed.get(CalculationService);

        expect(service).toBeTruthy();
    });
})