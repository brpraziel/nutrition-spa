import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class IngredientDataServices {
 
    public constructor(private readonly http: HttpClient) {}

    public getProductByDescription(productDescription:string): Observable<any> {
        return this.http.get<any>(`http://localhost:3000/product?productDescription=${productDescription}`);
      }
    
      public getProductByFoodgroup(foodgroup:string): Observable<any> {
        return this.http.get<any>(`http://localhost:3000/product/foodgroup?foodgroup=${foodgroup}`);
      }

      public deleteIngredient(id:string): Observable<any> {
       
        
        return this.http.delete<any>(`http://localhost:3000/ingredient/${id}`);
      }


}