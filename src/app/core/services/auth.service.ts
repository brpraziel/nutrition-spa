import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { UserRegister } from '../../common/models/user-register.dto';
import { UserLogin } from '../../common/models/user-login.dto';
import { StorageService } from '../services/storage.service';
import { tap } from 'rxjs/operators';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Router } from '@angular/router';
import { ShowUser } from '../../common/models/user-show.dto';
import { JWTService } from '../../auth/jwt.service';



@Injectable(
  {
    providedIn: 'root',
  }
)
export class AuthService {
  private readonly userSubject$ = new BehaviorSubject<string | null>(this.username);
  redirectUrl: string;
  isLoggedIn = false;

  public constructor(
    private readonly http: HttpClient,
    private readonly storage: StorageService,
    public jwtHelper: JwtHelperService,
    private router: Router,
    private readonly jwtService: JWTService,
  ) { }

  public get user$() {
    return this.userSubject$.asObservable();
  }

  private get username(): string | null {
    const token = this.storage.get('token');
    const username = this.storage.get('username') || '';
    if (token) {
      return username;
    }

    return null;
  }


  public login(user: UserLogin): Observable<any> {

    return this.http.post('http://localhost:3000/login', user).pipe(
      tap((res: any) => {
        
        this.isLoggedIn = true;
        let token4e:any =Object.values(res)
        this.storage.set('token', token4e);
        this.setUserId();
        this.isLoggedIn = true;
      }
      ));
  }

  public reverseToken(): any {
    const token = this.storage.get('token');
    if (!!token) {
      const decoded = this.jwtService.JWTDecoder(token);
      console.log(decoded);
      
      return decoded;
    }
    return '';
  }

  public setUserId(): void {
    const token: ShowUser = this.reverseToken();
   
    
    this.storage.set('userEmail', token.email);
  }

  public isAuthenticated(): boolean {
    const token = this.storage.get('token');
    // Check whether the token is expired and return
    // true or false
    if (!!token) {
      return !this.jwtHelper.isTokenExpired(token);
    }
    return false;
  }

}
