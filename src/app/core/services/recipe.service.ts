import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Recipe } from 'src/app/common/interfaces/recipe';

@Injectable({
  providedIn: 'root'
})
export class RecipeService {
  constructor(
    private readonly http: HttpClient,
  ) { }

  public getAllRecipes(): Observable<{recipes_list: Recipe[]}| any> {
    return this.http.get<{recipes_list: Recipe[]| any}>(`http://localhost:3000/recipe/`);
  }

  public getSingleRecipeByTitle(title: string): Observable<{recipe_details: Recipe[]}> {
    // const escapedTitle = title.replace('%20&%20', ' & '); // todo  -  escape '&'
    return this.http.get<{recipe_details: Recipe[]}>(`http://localhost:3000/recipe/?title=${title}`);
  }

  public getRecipeById(recipeId: string): Observable<Recipe> {
    return this.http.get<Recipe>(`http://localhost:3000/recipe/id/${recipeId}`);
  }

  public createRecipe(recipeBody: Recipe): Observable<Recipe> {
    
    return this.http.post<Recipe>(`http://localhost:3000/recipe`, recipeBody);
  }

  public archiveRecipeById(recipeId: string): Observable<{message: string}> {
    return this.http.delete<{message: string}>(`http://localhost:3000/recipe/${recipeId}`);
  }

  public editRecipe(id: string, recipeBody: Recipe): Observable<Recipe> {
    
    return this.http.put<Recipe>(`http://localhost:3000/recipe/${id}`, recipeBody);
  }
}
