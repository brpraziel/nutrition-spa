import { TestBed } from '@angular/core/testing';

import { AuthService } from './auth.service';
import { HttpClient } from '@angular/common/http';
import { StorageService } from './storage.service';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Router } from '@angular/router';
import { of } from 'rxjs';
import { UserRegister } from '../../common/models/user-register.dto';
import { JWTService } from '../../auth/jwt.service';
import { UserLogin } from '../../common/models/user-login.dto';

describe('AuthService', () => {
    const http = jasmine.createSpyObj('HttpClient', ['get', 'post']);
    const storage = jasmine.createSpyObj('StorageService', ['get', 'set', 'remove']);
    const jwtHelper = jasmine.createSpyObj('JwtHelperService', ['isTokenExpired']);
    const router = jasmine.createSpyObj('Router', ['navigate']);
    const jwtService = jasmine.createSpyObj('JWTService', ['JWTDecoder']);

    beforeEach(() => TestBed.configureTestingModule({
        providers: [
            {
                provide: HttpClient,
                useValue: http,
            },
            {
                provide: StorageService,
                useValue: storage,
            },
            {
                provide: JwtHelperService,
                useValue: jwtHelper,
            },
            {
                provide: Router,
                useValue: router,
            },
            {
                provide: JWTService,
                useValue: jwtService
            }
        ]
    }));

    it('should be created', () => {
        const service: AuthService = TestBed.get(AuthService);

        expect(service).toBeTruthy();
    });
    describe('login', () => {
        it('login should log the user, if correct data is passed', () => {
            const service: AuthService = TestBed.get(AuthService);
            const user: UserLogin = {
                email: 'mart@mart.com',
                password: 'password'
            };
            http.post.and.returnValue(of(user));
            router.navigate.calls.reset();


            service.login(user).subscribe(
                res => {
                    expect(res).toEqual(user);
                    expect(service.isLoggedIn).toBe(true);
                    expect(router.navigate).toHaveBeenCalledTimes(0);
                    expect(service.redirectUrl).toBe(undefined);
                }
            );
        });
    });
    describe('isAuthenticated', () => {
        it('isAuthenticated should return false when no token is set', () => {
            const service: AuthService = TestBed.get(AuthService);
            storage.get.and.returnValue('');

            const result = service.isAuthenticated();

            expect(result).toBe(false);
        });

        it('isAuthenticated should return false when the token has expired', () => {
            const service: AuthService = TestBed.get(AuthService);
            const token = { tokenHasExpired: true };
            storage.set.calls.reset();
            storage.get.and.returnValue(
                token
            );
            jwtHelper.isTokenExpired.and.returnValue(storage.get().tokenHasExpired);

            const result = service.isAuthenticated();

            expect(result).toBe(false);
        });

        it('isAuthenticated should return true when the token is valid', () => {
            const service: AuthService = TestBed.get(AuthService);
            const token = { tokenHasExpired: false };
            storage.set.calls.reset();
            storage.get.and.returnValue(
                token
            );
            jwtHelper.isTokenExpired.and.returnValue(storage.get().tokenHasExpired);

            const result = service.isAuthenticated();

            expect(result).toBe(true);
        });
    })
    describe('setUserId', () => {
        it('setUserId should set the userId', () => {
            const service: AuthService = TestBed.get(AuthService);
            const token = { email: 'mart@mart.com' };
            storage.set.calls.reset();
            storage.get.and.returnValue(
                token
            );
            jwtService.JWTDecoder.and.returnValue(
                token
            );

            service.setUserId();

            expect(storage.set).toHaveBeenCalledTimes(1);
            expect(storage.set).toHaveBeenCalledWith('userEmail', token.email);
        });
    });
    describe('reverseToken', () => {
        it('reverseToken should return decoded token', () => {
            const service: AuthService = TestBed.get(AuthService);
            const token = 'token';
            storage.set.calls.reset();
            storage.get.and.returnValue(
                token
            );
            jwtService.JWTDecoder.and.returnValue(storage.get());

            const result = service.reverseToken();

            expect(result).toBe('token');
        });

        it('reverseToken should return empty string, when token was not provided', () => {
            const service: AuthService = TestBed.get(AuthService);
            storage.get.and.returnValue('');

            const result = service.reverseToken();

            expect(result).toBe('');
        });

        it('reverseToken should call storage.get 1 time', () => {
            const service: AuthService = TestBed.get(AuthService);
            storage.get.calls.reset();

            service.reverseToken();

            expect(storage.get).toHaveBeenCalledTimes(1);
        });
    });
});
