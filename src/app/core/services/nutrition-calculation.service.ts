import { Injectable } from '@angular/core';
import { Recipe } from 'src/app/common/interfaces/recipe';
import { Ingredient } from 'src/app/common/interfaces/ingredient';
import { Nutrition } from 'src/app/common/interfaces/nutrition';
import { NutritionSummary } from 'src/app/common/interfaces/nutrition-summary';
import { Measure } from 'src/app/common/interfaces/measure';
import { NutritionFull } from 'src/app/common/interfaces/nutrition-full';

@Injectable({
  providedIn: 'root'
})
export class NutritionCalculationService {
  constructor() {}

  public calculateNutriSummaryForRecipe(recipe: Recipe): NutritionSummary {
    const _recipe: Recipe = recipe;
    const _ingredients: Ingredient[] = _recipe.ingredients;

    const ingredientNutriSum: NutritionSummary = {
      energy: 0,
      proteins: 0,
      carbs: 0,
      fat: 0,
    };

    _ingredients.map(
      (ingredient: Ingredient) => {
        const ingredientAmount: number = ingredient.amount;
        const selectedMeasure: string = ingredient.selectedMeasure; // todo : transform to base | grams
        const productMeasures: Measure[] = ingredient.product.measures;
        // const selectedMeasurePerHundredGrams: number =
        // NutritionCalculationService.getSelectedMeasureInGrams(selectedMeasure, productMeasures);

        const productNutrition: Nutrition = ingredient.product.nutrition[0];

        // in kcal - data per 100g
        const kcalVal = productNutrition.ENERC_KCAL.value;
        const proteinVal = productNutrition.PROCNT.value;
        const carbsVal = productNutrition.CHOCDF.value;
        const fatVal = productNutrition.FAT.value;

        // todo : add selected measure
        ingredientNutriSum.energy += kcalVal / 100 * (ingredientAmount * 1);
        ingredientNutriSum.proteins += proteinVal / 100 * (ingredientAmount * 1);
        ingredientNutriSum.carbs += carbsVal / 100 * (ingredientAmount * 1);
        ingredientNutriSum.fat += fatVal / 100 * (ingredientAmount * 1);
      }
    );

    return ingredientNutriSum;
  }


  public calculateFullNutritionForRecipe(recipe: Recipe): NutritionFull {
    const _recipe: Recipe = recipe;
    const _ingredients: Ingredient[] = _recipe.ingredients;

    const ingredientNutriFull: NutritionFull = {
      energy: 0,
      proteins: 0,
      carbs: 0,
      fat: 0,
      sugar: 0,
      fiber: 0,
      calcium: 0,
      iron: 0,
      phosphorus: 0,
      potassium: 0,
      sodium: 0,
      vitaminA: 0,
      vitaminE: 0,
      vitaminD: 0,
      vitaminC: 0,
      vitaminB12: 0,
      folicAcid: 0,
      cholesterol: 0,
    };

    _ingredients.map(
      (ingredient: Ingredient) => {
        const ingredientAmount: number = ingredient.amount;
        const selectedMeasure: string = ingredient.selectedMeasure; // todo : transform to base | grams
        const productMeasures: Measure[] = ingredient.product.measures;
        // const selectedMeasurePerHundredGrams: number =
        // NutritionCalculationService.getSelectedMeasureInGrams(selectedMeasure, productMeasures);
        const measure = 1;

        const productNutrition: Nutrition = ingredient.product.nutrition[0];

        // in kcal - data per 100g
        const kcalVal = productNutrition.ENERC_KCAL.value;
        const proteinVal = productNutrition.PROCNT.value;
        const carbsVal = productNutrition.CHOCDF.value;
        const fatVal = productNutrition.FAT.value;
        const sugarVal = productNutrition.SUGAR.value;
        const fiberVal = productNutrition.FIBTG.value;
        const calciumVal = productNutrition.CA.value;
        const ironVal = productNutrition.FE.value;
        const phosphorusVal = productNutrition.P.value;
        const potassiumVal = productNutrition.K.value;
        const sodiumVal = productNutrition.NA.value;
        const vitaminAVal = productNutrition.VITA_IU.value;
        const vitaminEVal = productNutrition.TOCPHA.value;
        const vitaminDVal = productNutrition.VITD.value;
        const vitaminCVal = productNutrition.VITC.value;
        const vitaminB12Val = productNutrition.VITB12.value;
        const folicAcidVal = productNutrition.FOLAC.value;
        const cholesterolVal = productNutrition.CHOLE.value;

        // todo : add selected measure
        ingredientNutriFull.energy += kcalVal / 100 * (ingredientAmount * measure);
        ingredientNutriFull.proteins += proteinVal / 100 * (ingredientAmount * measure);
        ingredientNutriFull.carbs += carbsVal / 100 * (ingredientAmount * measure);
        ingredientNutriFull.fat += fatVal / 100 * (ingredientAmount * measure);
        ingredientNutriFull.sugar += sugarVal / 100 * (ingredientAmount * measure);
        ingredientNutriFull.fiber += fiberVal / 100 * (ingredientAmount * measure);
        ingredientNutriFull.calcium += calciumVal / 100 * (ingredientAmount * measure);
        ingredientNutriFull.iron += ironVal / 100 * (ingredientAmount * measure);
        ingredientNutriFull.phosphorus += phosphorusVal / 100 * (ingredientAmount * measure);
        ingredientNutriFull.potassium += potassiumVal / 100 * (ingredientAmount * measure);
        ingredientNutriFull.sodium += sodiumVal / 100 * (ingredientAmount * measure);
        ingredientNutriFull.vitaminA += vitaminAVal / 100 * (ingredientAmount * measure);
        ingredientNutriFull.vitaminE += vitaminEVal / 100 * (ingredientAmount * measure);
        ingredientNutriFull.vitaminD += vitaminDVal / 100 * (ingredientAmount * measure);
        ingredientNutriFull.vitaminC += vitaminCVal / 100 * (ingredientAmount * measure);
        ingredientNutriFull.vitaminB12 += vitaminB12Val / 100 * (ingredientAmount * measure);
        ingredientNutriFull.folicAcid += folicAcidVal / 100 * (ingredientAmount * measure);
        ingredientNutriFull.cholesterol += cholesterolVal / 100 * (ingredientAmount * measure);
      }
    );

    return ingredientNutriFull;
  }

  // getSelectedMeasureInGrams(selectedMeasure: string, availableMeasurementsForProduct: Measure[]): number {
  //   const gramsPerMeasure: number = availableMeasurementsForProduct.filter(measure => {
  //     if (measure.measure === selectedMeasure) {
  //       return measure.gramsPerMeasure;
  //     }
  //   });
  //
  //   return gramsPerMeasure;
  // }

}
