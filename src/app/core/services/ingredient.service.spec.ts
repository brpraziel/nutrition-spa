import { TestBed } from '@angular/core/testing';
import { CalculationService } from './calculation.service';
import { IngredientDataServices } from './ingredient.service';
import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs';



describe('IngredientDataServices', () => {
    const http = jasmine.createSpyObj('HttpClient', ['get', 'delete']);
    const ingredientDataServices=jasmine.createSpyObj('IngredientDataServices',['getProductByDescription','getProductByFoodgroup'])
    beforeEach(() => TestBed.configureTestingModule({
        providers: [{
            provide: HttpClient,
                useValue: http,
        },
        {
            provide: IngredientDataServices,
            useValue: ingredientDataServices,
        },
        ]
    }));

    it('should be created', () => {
        const service: IngredientDataServices = TestBed.get(IngredientDataServices);

        expect(service).toBeTruthy();
    });
    it('getProductByDescription should return correct data', async () => {
        
        const productDescription="Butter, salted"
        const product={
            code:1001,
            description: "Butter, salted",
            foodGroup: "Dairy and Egg Products",
            measures:[],
            nutrition:[]
        }
        http.get.and.returnValue(of(product));
        const service: IngredientDataServices = TestBed.get(IngredientDataServices);
        ingredientDataServices.getProductByDescription.and.returnValue(of(product))
        
        service.getProductByDescription(productDescription).subscribe(
            (res)=>{
                expect(res).toEqual(product)
            }
            )
    });
    it('getProductByFoodgroup should return correct data', async () => {
        
        const productDescription="Dairy and Egg Products"
        const products=[]
        http.get.and.returnValue(of(products));
        const service: IngredientDataServices = TestBed.get(IngredientDataServices);
        ingredientDataServices.getProductByFoodgroup.and.returnValue(of(products))
        
        service.getProductByFoodgroup(productDescription).subscribe(
            (res)=>{
                expect(res).toEqual(products)
            }
            )
    });
    
})