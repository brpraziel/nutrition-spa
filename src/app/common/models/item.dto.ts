export class Item{
    NDB_No?: number
   FdGrp_Cd?: number
   Long_Desc?: string
   Shrt_Desc?: string
   ComName?: string
   ManufacName?: string
   Survey?: string
   Ref_desc?: string
   Refuse?: number
   SciName?: string
   N_Factor?: number
   Pro_Factor?: number
   Fat_Factor?: number
   CHO_Factor?: number
}