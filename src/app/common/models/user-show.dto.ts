export interface ShowUser {
    id: string;
    email: string;
  }