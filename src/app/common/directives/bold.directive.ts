import {
    Directive,
    ElementRef,
    Renderer2,
    HostListener,
    Input
  } from '@angular/core';

@Directive({
  selector: '[appBold]'
})
export class BoldDirective {
  public constructor(
    private readonly el: ElementRef,
    private readonly renderer: Renderer2
  ) {}

 // @Input() fontWeight: number;

  bold() {
    this.renderer.setStyle(
      this.el.nativeElement,
      'fontWeight',
      700
    );
  }
}
