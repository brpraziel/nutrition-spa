import { RecipeCategory } from '../enums/category.enum';

export interface SearchRecipe {
    title?: string;
    categories?: RecipeCategory;
}
