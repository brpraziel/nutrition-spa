// TODO : all interfaces need to be checked and if need be rewritten

import { Measure } from './measure';
import { Nutrition } from './nutrition';
// import { Ingredient } from './ingredient';

export interface Product {
    code?: number;
    description?: string;
    foodGroup?: string;
    measures?: Measure[];
    nutrition?: Nutrition[];
    // ingredients?: Ingredient[];
}
