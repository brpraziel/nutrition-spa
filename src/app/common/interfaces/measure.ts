// TODO : all interfaces need to be checked and if need be rewritten

import { Product } from './product';

export interface Measure {
    id: string;
    // product: Product;
    measure: string;
    gramsPerMeasure: number;
}
