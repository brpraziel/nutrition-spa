export interface NutritionFull {
    energy: number;
    fat: number;
    proteins: number;
    carbs: number;
    sugar: number;
    fiber: number;
    calcium: number;
    iron: number;
    phosphorus: number;
    potassium: number;
    sodium: number;
    vitaminA: number;
    vitaminE: number;
    vitaminD: number;
    vitaminC: number;
    vitaminB12: number;
    folicAcid: number;
    cholesterol: number;
}
