export interface SubrecipeDescriptor {
    subrecipeName: string;
    amount: string;
    overallNutrition: number;
}
