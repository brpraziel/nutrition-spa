export interface NutritionSummary {
    energy: number;
    fat: number;
    proteins: number;
    carbs: number;
}
