// TODO : all interfaces need to be checked and if need be rewritten

import { Ingredient } from './ingredient';
import { RecipeCategory } from '../enums/category.enum';

export interface Recipe {
  id?: string;
  title?: string;
  createdOn?: Date;
  ingredients?: Ingredient[];
  usedSubrecipesIds?: string[];
  categories?: RecipeCategory;
  overallNutrition?: number;
  information?: string;
}
