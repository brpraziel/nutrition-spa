// TODO : all interfaces need to be checked and if need be rewritten

import { Product } from './product';

export interface Ingredient {
    id?:string
    productName?: Product | any;
    amount?: number;
    product?: Product;
    selectedMeasure?: string;
}
