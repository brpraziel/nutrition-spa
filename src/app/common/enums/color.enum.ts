export enum CustomColors {
    blue = '#6593f5',
    green = '#aed407',
    grey = '#343A40',
}
