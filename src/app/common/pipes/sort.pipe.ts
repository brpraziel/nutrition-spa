import { Pipe, PipeTransform } from '@angular/core';
import { Recipe } from '../interfaces/recipe';
/*
 * Sorts recipes based on their kcal value per 100g
 * Takes a string argument that is 'asc' for ascending and 'desc' for descending
 * Usage:
 *   recipeArray | sort:order
 * Example:
 *   {{ 2 | sort:'desc' }}
*/
@Pipe({name: 'sort'})
export class SortByEnergyPipe implements PipeTransform {

    // @deprecated
    transform( recipes: Recipe[], direction: string ): Recipe[] {
        if (direction === 'asc') {
          return [...recipes].sort(
            (recipeOne, recipeTwo): any => {
                return recipeOne.overallNutrition - recipeTwo.overallNutrition;
            });
        } else {
            return [...recipes].sort(
              (recipeOne, recipeTwo): any => {
                return recipeTwo.overallNutrition - recipeOne.overallNutrition;
            });
        }
    }

}
