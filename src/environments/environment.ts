// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase:{
    apiKey: "AIzaSyCaoqHdbjsc82U11I19uJz3mQ2CdJZq-ro",
    authDomain: "nutrition-6fc5e.firebaseapp.com",
    databaseURL: "https://nutrition-6fc5e.firebaseio.com",
    projectId: "nutrition-6fc5e",
    storageBucket: "nutrition-6fc5e.appspot.com",
    messagingSenderId: "648773789813",
    appId: "1:648773789813:web:9a05ff5cf9d4b6c4"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
